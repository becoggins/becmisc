//  Copyright (c) 2006, 2007, 2012, Brian E. Coggins, Ph.D.  All rights reserved.

#ifndef BECMISC_PT_DIST_SPHERE
#define BECMISC_PT_DIST_SPHERE

#include <math.h>
#include <vector>
#include <boost/random.hpp>
#include "becmisc.h"

namespace pt_dist_sphere
{
    struct surface_pt
    {
        surface_pt( double azimuth_, double elevation_ ) : azimuth( azimuth_ ), elevation( elevation_ ) {  }
        surface_pt( double x_, double y_, double z_ )
        {
            double dist = sqrt( x * x + y * y + z * z );
			this->azimuth = atan2( y, x );
			this->elevation = BEC_Misc::pi / 2.0 - acos( z / dist );
        }
        
        double x()
        {
            return cos( this->azimuth ) * cos( this->elevation );
        }
        
        double y()
        {
            return sin( this->azimuth ) * cos( this->elevation );
        }
        
        double z()
        {
            return sin( this->elevation );
        }
        
        double azimuth;
        double elevation;
    };
    
    template< typename ForceEvaluator >
    std::vector< surface_pt > distribute_points_on_sphere( int n, ForceEvaluator fe, double k_product = 1.0, size_t max_iter = 0, unsigned int seed = 16u )
    {
        using namespace BEC_Misc;
        
        boost::mt19937 random_engine( seed );
		boost::uniform_on_sphere<> random_distributor( 3 );
		boost::variate_generator< boost::mt19937, boost::uniform_on_sphere<> > random_generator( random_engine, random_distributor );
        
        std::vector< surface_pt > points;
        
		for( int i = 0; i < n; i++ )
		{
			std::vector< double > coord = this->random_generator();
			double x = fast_abs( coord[ 0 ] ), y = fast_abs( coord[ 1 ] ), z = fast_abs( coord[ 2 ] );
            points.push_back( surface_pt( x, y, z ) );
		}
        
        double delta_pos = 1.0, delta_pos_old = 0.0, k = k_product / ( double ) n;
        size_t iter = 1;
        
        while( ( max_iter ? iter < max_iter : true ) && fast_abs( delta_pos ) > 0.00 )
        {
            std::vector< surface_pt > new_pts;
            
            for( std::vector< surface_pt >::iterator i = points.begin(); i != points.end(); ++i )
            {
                double x_force = 0.0, y_force = 0.0, z_force = 0.0;
                
                for( std::vector< surface_pt >::iterator j = points.begin(); j != points.end(); ++j )
                {
                    fe( *i, *j );
                    x_force += fe.x_force;
                    y_force += fe.y_force;
                    z_force += fe.z_force;
                }
                
                if( fast_abs( x_force ) > 1e7 || fast_abs( y_force ) > 1e7 || fast_abs( z_force ) > 1e7 )
					k_eff = k / sqrt( x_force * x_force + y_force * y_force + z_force * z_force );
                
                surface_pt new_pt( i->x() + k_eff * x_force, i->y() + k_eff * y_force, i->z() + k_eff * z_force );
                
                delta_pos += fast_abs( new_pt.x() - i->x() ) + fast_abs( new_pt.y() - i->y() ) + fast_abs( new_pt.z() - i->z() );
                
                new_pts.push_back( new_pt );
            }

            if( iter % 100 == 99 )
			{
				if( iter > 100 && delta_pos_old > delta_pos_old * 0.1 ) k *= 0.5;
				delta_pos_old = delta_pos;
			}
            
            points = new_pts;
            iter++;
        }
        
        return points;
    }
    
    struct force_evaluator_std
    {
        force_evaluator_std() : x_force( 0.0 ), y_force( 0.0 ), z_force( 0.0 )  {  }
        
        void operator()( surface_pt a, surface_pt b )
        {
            double x_diff = ax - bx;
			double y_diff = ay - by;
			double z_diff = az - bz;
            
			double dist = sqrt( x_diff * x_diff + y_diff * y_diff + z_diff * z_diff );
			double dist_cubed = dist * dist * dist;
            x_force = dist_cubed ? x_diff / dist_cubed : 0.0;
            y_force = dist_cubed ? y_diff / dist_cubed : 0.0;
            z_force = dist_cubed ? z_diff / dist_cubed : 0.0;
        }
        
        double x_force;
        double y_force;
        double z_force;
    }

    struct force_evaluator_octant_symmetry
    {
        force_evaluator_octant_symmetry() : x_force( 0.0 ), y_force( 0.0 ), z_force( 0.0 )  {  }
        
        void operator()( surface_pt a, surface_pt b )
        {
            x_force = 0.0;
            y_force = 0.0;
            z_force = 0.0;
            for( double axs = 1.0; axs >= -1.0; axs -= 2.0 )
                for( double ays = 1.0; ays >= -1.0; ays -= 2.0 )
                    for( double azs = 1.0; azs >= -1.0; azs -= 2.0 )
                        for( double bxs = 1.0; bxs >= -1.0; bxs -= 2.0 )
                            for( double bys = 1.0; bys >= -1.0; bys -= 2.0 )
                                for( double bzs = 1.0; bzs >= -1.0; bzs -= 2.0 )
                                {
                                    double x_diff = axs * ax - bxs * bx;
                                    double y_diff = ays * ay - bys * by;
                                    double z_diff = azs * az - bzs * bz;
                                    
                                    double dist = sqrt( x_diff * x_diff + y_diff * y_diff + z_diff * z_diff );
                                    double dist_cubed = dist * dist * dist;
                                    x_force += dist_cubed ? x_diff / dist_cubed : 0.0;
                                    y_force += dist_cubed ? y_diff / dist_cubed : 0.0;
                                    z_force += dist_cubed ? z_diff / dist_cubed : 0.0;
                                }
        }
        
        double x_force;
        double y_force;
        double z_force;
    }
}


#endif
