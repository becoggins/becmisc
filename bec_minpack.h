/*	bec_minpack.h
	Brian E. Coggins, 24 Aug 2007, rev. 7 Mar 2011, v.1.0.0
	Copyright (c) 2007-2011, Brian E. Coggins.  All rights reserved.

	BEC_MINPACK Library:  An interface to the classic MINPACK library 
	for nonlinear curve fitting.

	Please see the library documentation for details on the use of this
	library.

	This library may only be used or distributed under the terms of a
	license received from the author.  Please consult the accompanying
	license document to determine which rights and permissions have
	been granted to you.  If you did not receive a license document
	together with this file, you MUST contact the author to obtain a
	license before using or redistributing this file.
*/


#ifndef BEC_MINPACK_INCLUDED
#define BEC_MINPACK_INCLUDED

#include <vector>
#include <boost/function.hpp>
#include "becmisc.h"
#include "cminpack.h"


namespace BEC_MINPACK
{
	static const char *VersionString = "1.0.0";
	static const char *InitialDate = "24 Aug 2007";
	static const char *VersionReleaseDate = "7 Mar 2011";
	static const char *RevisionDate = "7 Mar 2011";
	static const char *Copyright = "Copyright (c) 2007-2011, Brian E. Coggins.  All rights reserved.";

	int fcn_lmdif( void *p, int m, int n, const double *x, double *fvec, int iflag );

	template< typename FunctionEvaluator >
	class FitDataToModel_LM_FiniteDiffJacobian
	{
	public:

		enum Result
		{
			ImproperInput = 0,
			ChiSqr_Acceptable = 1,
			RelErr_Acceptable = 2,
			ChiSqr_And_RelErr_Acceptable = 3,
			DifferencesOrthogonalToJacobian = 4,
			Timeout = 5,
			CannotLowerChiSqr = 6,
			CannotImproveSolution = 7
		};

		FitDataToModel_LM_FiniteDiffJacobian( int XValuesSize, int CoefsSize, FunctionEvaluator &Function ) 
			: m( 0 ), n( 0 ), x( 0 ), fvec( 0 ), iwa( 0 ), wa( 0 ), function( Function )
        {
            m = XValuesSize;
            n = CoefsSize;
            lwa = m * n + 5 * n + m;
            
            x = new double[ n ];
            fvec = new double[ m ];
            iwa = new int[ n ];
            wa = new double[ lwa ];
        }
        
        ~FitDataToModel_LM_FiniteDiffJacobian()
        {
            delete x;
            delete fvec;
            delete iwa;
            delete wa;
            
            x = 0;
            fvec = 0;
            iwa = 0;
            wa = 0;
        }

		Result operator()( const std::vector< double > &XValues, const std::vector< double > &YValues, const std::vector< double > &InitialCoefs, double Tolerance, std::vector< double > &Solution )
		{
			using namespace BEC_Misc;

			double tol = Tolerance;

			boost::function< int( int, int, const double *, double *, int ) > fcn_relay = fcn_processor( XValues, YValues, function );
			void *p = ( void * ) &fcn_relay;

			for( int i = 0; i < n; i++ )
				x[ i ] = InitialCoefs[ i ];

			for( int i = 0; i < m; i++ )
				fvec[ i ] = 0.0;

			int info = lmdif1( fcn_lmdif, p, m, n, x, fvec, tol, iwa, wa, lwa );

			for( int i = 0; i < n; i++ )
				Solution[ i ] = x[ i ];

			return ( Result ) info;
		}

	private:
		struct fcn_processor
		{
			fcn_processor( const std::vector< double > &XValues, const std::vector< double > &YValues, FunctionEvaluator &Function ) : xvalues( XValues ), yvalues( YValues ), function( Function ), coefs( XValues.size() )  {  }

			int operator()( int m, int n, const double *x, double *fvec, int iflag )
			{
				for( int i = 0; i < n; i++ )
					coefs[ n ] = x[ i ];

				for( int i = 0; i < m; i++ )
					fvec[ i ] = yvalues[ i ] - function( xvalues[ i ], coefs );

				return 0;
			}

			const std::vector< double > &xvalues;
            const std::vector< double > &yvalues;
            std::vector< double > coefs;
            FunctionEvaluator &function;
		};
        
        int m;
        int n;
        int lwa;
        
        double *x;
        double *fvec;
        int *iwa;
        double *wa;

		FunctionEvaluator &function;
	};

	inline int fcn_lmdif( void *p, int m, int n, const double *x, double *fvec, int iflag )
	{
		boost::function< int( int, int, const double *, double *, int ) > *fcn_relay = 
			( boost::function< int( int, int, const double *, double *, int ) > * ) p;
		return ( *fcn_relay )( m, n, x, fvec, iflag );
	}

	int fcn_lmder( void *p, int m, int n, const double *x, double *fvec, double *fjac, int ldfjac, int iflag );

	template< typename FunctionEvaluator >
	class FitDataToModel_LM_AnalyticJacobian
	{
	public:

		enum Result
		{
			ImproperInput = 0,
			ChiSqr_Acceptable = 1,
			RelErr_Acceptable = 2,
			ChiSqr_And_RelErr_Acceptable = 3,
			DifferencesOrthogonalToJacobian = 4,
			Timeout = 5,
			CannotLowerChiSqr = 6,
			CannotImproveSolution = 7
		};

		FitDataToModel_LM_AnalyticJacobian( int XValuesSize, int CoefsSize, FunctionEvaluator &Function, int MaxIterations ) 
        : m( 0 ), n( 0 ), x( 0 ), fvec( 0 ), iwa( 0 ), wa( 0 ), function( Function ), maxfev( MaxIterations )
        {
            m = XValuesSize;
            n = CoefsSize;
            lwa = m * n + 5 * n + m;
            
            x = new double[ n ];
            fvec = new double[ m ];
            fjac = new double[ m * n ];
            diag = new double[ n ];
            ipvt = new int[ n ];
            qtf = new double[ n ];
            wa1 = new double[ n ];
            wa2 = new double[ n ];
            wa3 = new double[ n ];
            wa4 = new double[ m ];
            iwa = new int[ n ];
            wa = new double[ lwa ];
        }
        
        ~FitDataToModel_LM_AnalyticJacobian()
        {
            delete x;
            delete fvec;
            delete fjac;
            delete diag;
            delete ipvt;
            delete qtf;
            delete wa1;
            delete wa2;
            delete wa3;
            delete wa4;
            delete iwa;
            delete wa;
        }


		Result operator()( const std::vector< double > &XValues, const std::vector< double > &YValues, const std::vector< double > &InitialCoefs, double Tolerance, std::vector< double > &Solution )
		{
			using namespace BEC_Misc;

			double tol = Tolerance;

            //  fcn: user-supplied callback for calculating the value of the function to be minimized and its Jacobian
            //
            //  In CMINPACK, there is an extra parameter p, a void pointer that is passed to the callback as its first argument.
            //  This allows us to structure the callback as a functor, even though a functor could not normally be passed as a
            //  function pointer.  Instead, we pass a pointer to the function fcn_lmder, which casts p to a boost::function
            //  and calls its operator().  As setup for this, we must prepare the boost::functor, which is here stored in the local
            //  variable fcn_relay.  It calls fcn_processor, which in turn calls the functor supplied by the client code.
			boost::function< int( int, int, const double *, double *, double *, int, int ) > fcn_relay = fcn_processor( XValues, YValues, function );
			void *p = ( void * ) &fcn_relay;
            
            //  m: number of data values against which the function should be minimized (input)
            //  preset in constructor
            
            //  n: number of coefficients to optimize in order to minimize the error with the data values (input)
            //  preset in constructor

            //  x: initial guess on input, final solution on output
			for( int i = 0; i < n; i++ )
				x[ i ] = InitialCoefs[ i ];

            //  fvec: stores the final values of the minimized function corresponding to the final solution stored in x (output)
			for( int i = 0; i < m; i++ )
				fvec[ i ] = 0.0;

            //  fjac: stores the final Jacobian and other calculation info; see lmder documentation for details (output)
			for( int i = 0; i < m * n; i++ )
				fjac[ i ] = 0.0;

            //  ldfjac: positive integer of not less than m specifying the leading dimension of the array fjac (input)
			int ldfjac = m;

            //  ftol: the relative error desired in the sum of squares (input)
            double ftol = Tolerance;
            
            //  xtol: the relative error desired in the approximate solution (input)
            double xtol = Tolerance;
            
            //  gtol: the orthogonality desired between the function vector and the columns of the Jacobian (input)
            double gtol = 0.0;
            
            //  maxfev: maximum number of iterations
            //  preset in constructor
            
            //  diag: multiplicative scale factors for the variables, set automatically if mode = 1, otherwise must supply (input)
            //  since mode is 1, we do not need to initialize this
            
            //  mode: whether to scale the variables automatically (1), or to use the diag parameter to get the scaling (2) (input)
            int mode = 1;
            
            //  factor: used in determining the initial step bound.  In most cases should lie between 1 and 100, with 100 recommended (input)
            double factor = 100.0;
            
            //  nprint: if positive, indicates that fcn should be called every nprint iterations and at final iteration to allow the current solution to be printed or otherwise reported (input)
            int nprint = 0;
            
            //  info: result of calculation, corresponding to one of the codes in the enum (originally an output parameter at this point in the calling sequence; in CMINPACK, this is the return value)
            
            //  nfev: number of calls to fcn to determine the function value (output)
            int nfev = 0;
            
            //  njev: number of calls to fcn to determine the Jacobian (output)
            int njev = 0;
            
            //  ipvt: permutation matrix p such that the final Jacobian * p = an orthogonal matrix q * an upper triangular matrix r (output)
            //  since this is output, we don't need to initialize it
            
            //  qtf: the first n elements of the vector (q transpose) * fvec (output)
            //  since this is output, we don't need to initialize it
            
            //  wa1, wa2, wa3: work arrays of length n
            //  wa4: work array of length m
            //  no need to initialize
            
			//int info = lmder1( fcn_lmder, p, m, n, x, fvec, fjac, ldfjac, tol, iwa, wa, lwa );
            int info = lmder( fcn_lmder, p, m, n, x, fvec, fjac, ldfjac, ftol, xtol, gtol, maxfev, diag, mode, factor, nprint, &nfev, &njev, ipvt, qtf, wa1, wa2, wa3, wa4 );

            //  retrieve solution from x
			for( int i = 0; i < n; i++ )
				Solution[ i ] = x[ i ];
            
			return ( Result ) info;
		}

	private:
		struct fcn_processor
		{
			fcn_processor( const std::vector< double > &XValues, const std::vector< double > &YValues, FunctionEvaluator &Function ) : xvalues( XValues ), yvalues( YValues ), function( Function ), coefs( XValues.size() ), J( XValues.size() )  {  }
            
			int operator()( int m, int n, const double *x, double *fvec, int iflag )
			{
				for( int i = 0; i < n; i++ )
					coefs[ i ] = x[ i ];
                
				for( int i = 0; i < m; i++ )
					fvec[ i ] = yvalues[ i ] - function( xvalues[ i ], coefs );
                
				return 0;
			}
            
			int operator()( int m, int n, const double *x, double *fvec, double *fjac, int ldfjac, int iflag )
			{
				for( int i = 0; i < n; i++ )
					coefs[ i ] = x[ i ];

				if( iflag == 1 )
				{
					//	Calculate residuals and store in fvec
					for( int i = 0; i < m; i++ )
						fvec[ i ] = yvalues[ i ] - function( xvalues[ i ], coefs );
				}
				else if( iflag == 2 )
				{
					//	Calculate Jacobian and store in fjac
					//	fjac should have m columns and n rows.  The value at column i, row j should be:
					//		fjac(i, j) = [ pd( yi ) / pd( cj ) ]( xi )
					//	where pd represents the partial derivative,
					//	      yi represents the ith y data value,
					//	      xi represents the ith x data value,
					//	      cj represents the jth coefficient
					//	Note that MINPACK uses FORTRAN-style column-major arrays, so fjac must be organized that way
					//	The FunctionEvaluator should have a function Jacobian( x, coefs, J ) that computes this at x 
					//	  and puts the result in J, such that after Jacobian( x[ i ], coefs, J ), J[ j ] gives fjac(i, j).
					for( int i = 0; i < m; i++ )
					{
						function.Jacobian( xvalues[ i ], coefs, J );
						for( int j = 0; j < n; j++ )
							fjac[ j * m + i ] = J[ j ];
					}
				}

				return 0;
			}

			const std::vector< double > &xvalues;
            const std::vector< double > &yvalues;
            std::vector< double > coefs;
            std::vector< double > J;
            FunctionEvaluator &function;
		};

        int m;
        int n;
        int maxfev;
        int lwa;

        double *x;
        double *fvec;
        double *fjac;
        double *diag;
        int *ipvt;
        double *qtf;
        double *wa1, *wa2, *wa3, *wa4;
        int *iwa;
        double *wa;
        
		FunctionEvaluator &function;
	};

	inline int fcn_lmder( void *p, int m, int n, const double *x, double *fvec, double *fjac, int ldfjac, int iflag )
	{
		boost::function< int( int, int, const double *, double *, double *, int, int ) > *fcn_relay = 
			( boost::function< int( int, int, const double *, double *, double *, int, int ) > * ) p;
		return ( *fcn_relay )( m, n, x, fvec, fjac, ldfjac, iflag );
	}

	struct DualGaussianFunctionEvaluator
	{
		double operator()( double x, const std::vector< double > &coefs )
		{
			double a1 = coefs[ 0 ];
			double mu1 = coefs[ 1 ];
			double sigma1 = coefs[ 2 ];
			double a2 = coefs[ 3 ];
			double mu2 = coefs[ 4 ];
			double sigma2 = coefs[ 5 ];

			return a1 * exp( -1.0 * ( x - mu1 ) * ( x - mu1 ) / ( 2.0 * sigma1 * sigma1 ) ) +
				a2 * exp( -1.0 * ( x - mu2 ) * ( x - mu2 ) / ( 2.0 * sigma2 * sigma2 ) );
		}

		void Jacobian( double x, const std::vector< double > &coefs, std::vector< double > &J )
		{
			double a1 = coefs[ 0 ];
			double mu1 = coefs[ 1 ];
			double sigma1 = coefs[ 2 ];
			double a2 = coefs[ 3 ];
			double mu2 = coefs[ 4 ];
			double sigma2 = coefs[ 5 ];

			J[ 0 ] = -exp( -( -mu1 + x ) * ( -mu1 + x ) / ( 2.0 * sigma1 * sigma1 ) );
			J[ 1 ] = -a1 * exp( -( -mu1 + x ) * ( -mu1 + x ) / ( 2.0 * sigma1 * sigma1 ) ) * ( -mu1 + x ) / ( sigma1 * sigma1 );
			J[ 2 ] = -a1 * exp( -( -mu1 + x ) * ( -mu1 + x ) / ( 2.0 * sigma1 * sigma1 ) ) * ( -mu1 + x ) * ( -mu1 + x ) / ( sigma1 * sigma1 * sigma1 );
			J[ 3 ] = -exp( -( -mu2 + x ) * ( -mu2 + x ) / ( 2.0 * sigma2 * sigma2 ) );
			J[ 4 ] = -a2 * exp( -( -mu2 + x ) * ( -mu2 + x ) / ( 2.0 * sigma2 * sigma2 ) ) * ( -mu2 + x ) / ( sigma2 * sigma2 );
			J[ 5 ] = -a2 * exp( -( -mu2 + x ) * ( -mu2 + x ) / ( 2.0 * sigma2 * sigma2 ) ) * ( -mu2 + x ) * ( -mu2 + x ) / ( sigma2 * sigma2 * sigma2 );
		}
	};

	struct GaussianFunctionEvaluator
	{
		double operator()( double x, const std::vector< double > &coefs )
		{
			double a = coefs[ 0 ];
			double mu = coefs[ 1 ];
			double sigma = coefs[ 2 ];

			return a * exp( -1.0 * ( x - mu ) * ( x - mu ) / ( 2.0 * sigma * sigma ) );
		}

		void Jacobian( double x, const std::vector< double > &coefs, std::vector< double > &J )
		{
			double a = coefs[ 0 ];
			double mu = coefs[ 1 ];
			double sigma = coefs[ 2 ];

			J[ 0 ] = -exp( -( -mu + x ) * ( -mu + x ) / ( 2.0 * sigma * sigma ) );
			J[ 1 ] = -a * exp( -( -mu + x ) * ( -mu + x ) / ( 2.0 * sigma * sigma ) ) * ( -mu + x ) / ( sigma * sigma );
			J[ 2 ] = -a * exp( -( -mu + x ) * ( -mu + x ) / ( 2.0 * sigma * sigma ) ) * ( -mu + x ) * ( -mu + x ) / ( sigma * sigma * sigma );
		}
	};

}

#endif		// BEC_MINPACK_INCLUDED

/*  End bec_minpack.h  */
