#import <Cocoa/Cocoa.h>
#import <Growl/Growl.h>
#include "bec_growl.h"

@interface growl_delegate : NSObject <GrowlApplicationBridgeDelegate>
{
    
}
- ( NSDictionary * ) registrationDictionaryForGrowl;
@end

namespace BEC_Growl
{

    struct details::gn_delegate_owner
    {
        gn_delegate_owner( void )
        {
            gd = [[growl_delegate alloc] init];
            [GrowlApplicationBridge setGrowlDelegate:gd];
        }
        
        ~gn_delegate_owner( void )
        {
            [gd release];
        }
        
        growl_delegate *gd;
    };


    GrowlNotifier::GrowlNotifier( void )
    {
        d_own = new details::gn_delegate_owner;
    }
    
    GrowlNotifier::~GrowlNotifier( void )
    {
        delete d_own;
    }
    
    void GrowlNotifier::Notify( std::string Title, std::string Message, bool IsSticky )
    {
        NSString *title = [NSString stringWithCString:Title.c_str()
                             encoding:[NSString defaultCStringEncoding]];
        NSString *msg = [NSString stringWithCString:Message.c_str()
                                             encoding:[NSString defaultCStringEncoding]];
        
        [GrowlApplicationBridge
         notifyWithTitle:title
         description:msg
         notificationName:@"Notification"
         iconData: nil
         priority:1
         isSticky:( IsSticky ? YES : NO )
         clickContext:nil];
    }

}


@implementation growl_delegate

- ( NSDictionary * ) registrationDictionaryForGrowl
{
    NSArray *notifications = [NSArray arrayWithObject: @"Notification"];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          notifications, GROWL_NOTIFICATIONS_ALL,
                          notifications, GROWL_NOTIFICATIONS_DEFAULT, nil];
    
    return dict;
}

@end
