/*	becmisc.h
	Brian E. Coggins, 18 Aug 2004, rev. 6 Aug 2009, v.1.0.2
	Copyright (c) 2004-2009, Brian E. Coggins.  All rights reserved.

	BECMisc Library:  A library of miscellaneous classes and functions.

	Please see the library documentation for details on the use of this
	library.

	This library may only be used or distributed under the terms of a
	license received from the author.  Please consult the accompanying
	license document to determine which rights and permissions have
	been granted to you.  If you did not receive a license document
	together with this file, you MUST contact the author to obtain a
	license before using or redistributing this file.
*/

/**	\file
	\brief Miscellaneous classes and templates.

	This file contains a set of miscellaneous classes and templates supporting
	general programming needs, including smart pointers and pointer containers.
*/

#ifndef BEC_MISC_INCLUDED
#define BEC_MISC_INCLUDED

#ifdef INCLUDE_KBRT
#ifdef WIN32
#include <conio.h>
#else
#include <termios.h>
#include <unistd.h>
#endif
#endif

#include <math.h>
#include <memory.h>
#include <string.h>
#include <stddef.h>
#include <iterator>
#include <vector>
#include <functional>
#include <string>
#include <valarray>
#include <limits>
#include <complex>
#include <queue>
#include <map>
#include <memory>

///	Miscellaneous classes and templates.
/**	Smart pointers:  The stack_ptr and stack_array_ptr classes are designed
	to sit on the stack and own a single heap-allocated object or array, 
	respectively.

	Pointer containers:  Four classes are provided for managing collections of
	pointers to heap-allocated objects.  These classes are built on the model
	of the STL vector container, but are adapted to deal with the unusual aspects
	of managing heap-allocated entities.  The owned_ptr_vector class is the basic
	starting point: it holds pointers to individual objects, which it "owns",
	deallocating them upon its own destruction or whenever they are removed from
	the container.  This class is designed to sit on the stack, and to preserve
	its collection of objects until it goes out of scope.  The owned_array_ptr_vector
	has the same function, except for arrays.  The ptr_vector and array_ptr_vector
	classes are equivalent to owned_ptr_vector and owned_array_ptr_vector, but do
	not hold ownership of their objects.  They are designed for the purpose of
	passing a collection of objects/arrays to another procedure to be used
	temporarily.  They do not delete their contents upon going out of scope.

	String manipulation:  str_tok is a basic string parser, which splits a string
	into tokens.  It uses a modern C++ interface, and supports the kinds of features
	found in most tokenizing libraries as well as the ancient strtok() function.
	StrTok is a primitive string parser, which directly models the traditional C 
	library function strtok().  This class is provided for compatibility with existing
	code, but its use in new code is strongly discouraged, and it is formally listed
	as deprecated.  tmpstr holds a heap-allocated string temporarily, deallocating 
	the string upon its own destruction.  The tmpstr class behaves like a char* and 
	converts readily to a char*, except that it cleans up after itself.

	Predicates:  The abs_less and abs_greater classes are predicates designed for
	use in STL algorithms.  They evaluate which of two values has a smaller or
	larger absolute value, respectively.

	Binders:  The binding function templates are designed to work like the bind1st
	and bind2nd standard library binding functions, except handling other
	combinations of arguments.  The only one provided right now is called
	bind_constant, and has the purpose of "binding a constant," i.e. producing a
	functor that always returns a constant value.  This binder is also capable
	of receiving (and ignoring) one or two arguments, so it can pretend to be a
	unary or binary function.

	Mathematical Functions and Constants:  A variety of miscellaneous math functions
	and utilities have been included.

*/
namespace BEC_Misc
{
	static const char *VersionString = "1.0.2";
	static const char *InitialDate = "1 Aug 2006";
	static const char *VersionReleaseDate = "6 Aug 2009";
	static const char *RevisionDate = "6 Aug 2009";
	static const char *Copyright = "Copyright (c) 2004-2009, Brian E. Coggins.  All rights reserved.";

	template< typename T > struct DefaultObjectDeallocator;
	template< typename T > struct ObjectDeallocatorOptionalDelete;
	template< typename T > struct DefaultArrayDeallocator;
	template< typename T > struct ArrayDeallocatorOptionalDelete;

	///	Stack-based smart pointer to a single heap-allocated object.
	/**	This class is designed to sit on the stack and "own" a single 
		heap-allocated object, which it deletes automatically upon
		destruction.  The object to own is normally supplied through
		the constructor, and the object's lifetime is subsequently
		controlled by the lifetime of the stack_ptr.  The stack_ptr is
		designed to convert to a T* automatically, facilitating easy
		use in most contexts.  It also implements the normal pointer
		dereferencing operators, so it can be used like a normal
		pointer.

		The stack_ptr class is expressly designed to be noncopyable,
		unlike the std::auto_ptr and other smart pointers.  It owns
		its object, and it is not intended to give up its ownership
		except in extreme circumstances.  Note that, as a consequence,
		initialization must be done by an explicit call to the
		constructor.

		Assignment semantics are supported.  Upon assignment of a new
		pointer to a stack_ptr, any object already owned is deallocated,
		and ownership of the new object is assumed.

		Deletion of owned objects is managed through a deallocator
		object.  The type of the deallocator can be specified as a template
		parameter, and an instance of a deallocator can be supplied
		in the class constructor.  In most cases, these parameters can be
		omitted, in which case the default deallocator 
		(DefaultObjectDeallocator) will be used.  This class calls
		operator delete to deallocate the object.  In cases requiring
		more complicated deallocation requirements, the user may define
		and supply a custom deallocator.  This class is only required to
		implement a single public member, dealloc(), which is able to
		carry out the deallocation given a pointer of type T.  Note that
		deallocators are held by stack_ptr using value semantics and
		should be copyable.

		Although it should not be used routinely, an interface is included
		for releasing an object from a stack_ptr, and for giving a stack_ptr
		control of an object after creation.  Use set() and release() for this
		purpose.  The deallocator instance can also be changed while running
		using set_dealloc().

		\param		T		The type of object to be referenced.
		\param		DeallocatorClass	The type of the deallocator object to use.
	*/
	template< typename T, typename DeallocatorClass = DefaultObjectDeallocator< T > >
	class stack_ptr
	{
		public:
			///	Constructs an empty stack_ptr, optionally with a custom deallocator.
			stack_ptr( DeallocatorClass deallocator = DeallocatorClass() ) : dealloc( deallocator ), ptr( 0 ) {  }

			///	Constructs a stack_ptr owning the object referenced by ptr, optionally with a custom deallocator.
			stack_ptr( T *ptr, DeallocatorClass deallocator = DeallocatorClass() ) : dealloc( deallocator )
			{
				this->ptr = ptr;
			}

			~stack_ptr( void )
			{
				this->dealloc.dealloc( this->ptr );
			}

			///	Assigns a free object to the stack_ptr, to be owned and managed by the same in the future.
			/**	\note	Any object already held by the stack_ptr will be deleted.
			*/
			stack_ptr< T, DeallocatorClass > &operator=( T *ptr )
			{
				this->dealloc.dealloc( this->ptr );
				this->ptr = ptr;
				return *this;
			}

			///	Converts the stack_ptr to a simple pointer.
			/**	\warning	This conversion produces a pointer to an object of
							limited lifetime.  Be careful not to use the pointer
							after the stack_ptr has deleted the object.

				\warning	The pointer returned from this conversion should not
							be deleted.
			*/
			operator T*()
			{
				return this->ptr;
			}

			T &operator*( void )
			{
				return *( this->ptr );
			}

			T *operator->( void )
			{
				return this->ptr;
			}

			///	Converts the stack_ptr to a simple pointer.
			/**	\warning	This conversion produces a pointer to an object of
							limited lifetime.  Be careful not to use the pointer
							after the stack_ptr has deleted the object.

				\warning	The pointer returned from this conversion should not
							be deleted.
			*/
			T *get( void )
			{
				return this->ptr;
			}

			///	Retrieves a reference to the deallocator object.
			/**	Note that this function obtains a reference to the deallocator,
				rather than a copy of it.
			*/
			DeallocatorClass &get_dealloc( void )
			{
				return dealloc;
			}

			///	Assigns a free object to the stack_ptr, to be owned and managed by the same in the future.
			/**	\note	Any object already held by the stack_ptr will be deleted.
			*/
			void set( T *ptr )
			{
				this->dealloc.dealloc( this->ptr );
				this->ptr = ptr;
			}

			///	Assigns a free object to the stack_ptr, while also supplying a new deallocator.
			/**	\note	Any object already held by the stack_ptr will be deleted.
			*/
			void set( T *ptr, DeallocatorClass newdealloc )
			{
				this->dealloc.dealloc( this->ptr );
				this->dealloc = newdealloc;
				this->ptr = ptr;
			}

			///	Sets a new deallocator.
			void set_dealloc( DeallocatorClass newdealloc )
			{
				this->dealloc = newdealloc;
			}

			///	Releases ownership of the object.
			/**	This function returns a simple pointer to the object previously owned.  The
				stack_ptr will thereafter contain a NULL value.  The caller assumes
				responsibility for properly deallocating the object.
			*/
			T *release( void )
			{
				T *tmp = this->ptr;
				this->ptr = 0;
				return tmp;
			}

		private:
			stack_ptr( stack_ptr< T > &src );
			stack_ptr< T, DeallocatorClass > &operator=( stack_ptr &src );

			T *ptr;
			DeallocatorClass dealloc;
	};

	///	Stack-based smart pointer to a single heap-allocated array.
	/**	This class is designed to sit on the stack and "own" a 
		heap-allocated array, which it deletes automatically upon
		destruction.  The array to own is normally supplied through
		the constructor, and the array's lifetime is subsequently
		controlled by the lifetime of the stack_array_ptr.  The 
		stack_array_ptr is designed to convert to a T* automatically, 
		facilitating easy use in most contexts.  It also implements 
		the normal pointer dereferencing operators, so it can be used 
		like a normal pointer.

		The stack_array_ptr class is expressly designed to be noncopyable,
		unlike the std::auto_ptr and other smart pointers.  It owns
		its array, and it is not intended to give up its ownership
		except in extreme circumstances.  Note that, as a consequence,
		initialization must be done by an explicit call to the
		constructor.

		Assignment semantics are supported.  Upon assignment of a new
		pointer to a stack_array_ptr, any array already owned is deallocated,
		and ownership of the new array is assumed.

		Deletion of owned arrays is managed through a deallocator
		object.  The type of the deallocator can be specified as a template
		parameter, and an instance of a deallocator can be supplied
		in the class constructor.  In most cases, these parameters can be
		omitted, in which case the default deallocator 
		(DefaultArrayDeallocator) will be used.  This class calls
		operator delete [] to deallocate the object.  In cases requiring
		more complicated deallocation requirements, the user may define
		and supply a custom deallocator.  This class is only required to
		implement a single public member, dealloc(), which is able to
		carry out the deallocation given a pointer of type T.  Note that
		deallocators are held by stack_array_ptr using value semantics and
		should be copyable.

		Although it should not be used routinely, an interface is included
		for releasing an array from a stack_array_ptr, and for giving a 
		stack_array_ptr	control of an array after creation.  Use set() and 
		release() for this purpose.  The deallocator instance can also be 
		changed while running using set_dealloc().

		\param		T		The type of object contained by the array.
		\param		DeallocatorClass	The type of the deallocator object to use.
	*/
	template< typename T, typename DeallocatorClass = DefaultArrayDeallocator< T > >
	class stack_array_ptr
	{
		public:
			///	Constructs an empty stack_array_ptr, optionally with a custom deallocator.
			stack_array_ptr( DeallocatorClass deallocator = DeallocatorClass() ) : dealloc( deallocator ), ptr( 0 ) {  }

			///	Constructs a stack_array_ptr owning the array referenced by ptr, optionally with a custom deallocator.
			stack_array_ptr( T *ptr, DeallocatorClass deallocator = DeallocatorClass() ) : dealloc( deallocator )
			{
				this->ptr = ptr;
			}

			~stack_array_ptr( void )
			{
				this->dealloc.dealloc( this->ptr );
			}

			///	Assigns a free array to the stack_ptr, to be owned and managed by the same in the future.
			/**	\note	Any array already held by the stack_ptr will be deleted.
			*/
			stack_array_ptr< T, DeallocatorClass > &operator=( T *ptr )
			{
				this->dealloc.dealloc( this->ptr );
				this->ptr = ptr;
				return *this;
			}

			///	Converts the stack_array_ptr to a simple pointer.
			/**	\warning	This conversion produces a pointer to an array of
							limited lifetime.  Be careful not to use the pointer
							after the stack_ptr has deleted the array.

				\warning	The pointer returned from this conversion should not
							be deleted.
			*/
			operator T*()
			{
				return this->ptr;
			}

			T &operator*( void )
			{
				return *( this->ptr );
			}

			T *operator->( void )
			{
				return this->ptr;
			}

			///	Converts the stack_array_ptr to a simple pointer.
			/**	\warning	This conversion produces a pointer to an array of
							limited lifetime.  Be careful not to use the pointer
							after the stack_ptr has deleted the array.

				\warning	The pointer returned from this conversion should not
							be deleted.
			*/
			T *get( void )
			{
				return this->ptr;
			}

			///	Retrieves a reference to the deallocator object.
			/**	Note that this function obtains a reference to the deallocator,
				rather than a copy of it.
			*/
			DeallocatorClass &get_dealloc( void )
			{
				return this->dealloc;
			}

			///	Assigns a free array to the stack_ptr, to be owned and managed by the same in the future.
			/**	\note	Any array already held by the stack_ptr will be deleted.
			*/
			void set( T *ptr )
			{
				this->dealloc.dealloc( this->ptr );
				this->ptr = ptr;
			}

			///	Assigns a free array to the stack_ptr, while also supplying a new deallocator.
			/**	\note	Any array already held by the stack_ptr will be deleted.
			*/
			void set( T *ptr, DeallocatorClass newdealloc )
			{
				this->dealloc.dealloc( this->ptr );
				this->dealloc = newdealloc;
				this->ptr = ptr;
			}

			///	Sets a new deallocator.
			void set_dealloc( DeallocatorClass newdealloc )
			{
				this->dealloc = newdealloc;
			}

			///	Releases ownership of the array.
			/**	This function returns a simple pointer to the array previously owned.  The
				stack_ptr will thereafter contain a NULL value.  The caller assumes
				responsibility for properly deallocating the array.
			*/
			T *release( void )
			{
				T *tmp = this->ptr;
				this->ptr = 0;
				return tmp;
			}

		private:
			stack_array_ptr( stack_array_ptr< T > &src );
			stack_array_ptr< T, DeallocatorClass > &operator=( stack_array_ptr &src );

			T *ptr;
			DeallocatorClass dealloc;
	};

	template< typename T > struct DefaultObjectFactory;
	
	template< typename T, typename ObjectFactoryClass = DefaultObjectFactory< T > > class owned_ptr_vector;

	///	Stores a collection of pointers to objects, but does not manage those objects' lifetimes.
	/**	The ptr_vector class behaves like the STL std::vector class, but stores
		pointers to objects instead of the objects themselves.  ptr_vector does
		<em>not</em> take responsibility for the proper deallocation of the objects
		that its pointers reference; it is intended as a way of moving a collection
		of pointers that are owned elsewhere.  The owned_ptr_vector is designed to
		own a collection of objects and manage their proper deallocation.

		Pointer collections are ideal for storing/referencing polymorphic objects, which
		cannot easily be stored by value.  The ptr_vector can be declared with
		the type parameter set to the type of the base class, and when stored objects
		are accessed, the features of the different derived classes are used rather
		than those of the base classes (compare a std::vector, where adding a derived
		class to a std::vector of the base class causes slicing and data corruption).

		The ptr_vector interface is nearly identical to that of the STL std::vector.
		However, the interface is designed so that the normal dereferencing operators
		return references to the objects themselves, rather than the stored pointers to 
		the	objects.  Thus accessing objects stored by reference in a ptr_vector has the 
		same semantics as accessing objects stored by value in std::vector.  A separate 
		set of member functions are provided for obtaining the pointers themselves.

		The documentation below only describes in detail those features which differ
		from the STL std::vector API.  Please see an STL reference for more information
		about the details of the interface.

		Unlike the owned_ptr_vector, the ptr_vector is freely copyable.

		The ptr_vector is designed to be usable in conjunction with instances of the 
		owned_ptr_vector class. In order to facilitate interoperability, it is necessary 
		for the ptr_vector to take the type parameter ObjectFactoryClass.  In most cases 
		this should be left to the default, but in cases where the ptr_vector must interact 
		with an	owned_ptr_vector that has a nonstandard ObjectFactoryClass, the ptr_vector
		must be declared with an ObjectFactoryClass that matches the ObjectFactoryClass
		of the owned_ptr_vector.  However, the ObjectFactoryClass is not otherwise used
		by the ptr_vector, and has no effect on its operation.

		\param	T	The type of the object to which the stored pointers point.
		\param	ObjectFactoryClass	When interacting with owned_ptr_vector objects, should
									be set to match the ObjectFactoryClass of the 
									owned_ptr_vector.
	*/
	template< typename T, typename ObjectFactoryClass = DefaultObjectFactory< T > >
	class ptr_vector
	{
		friend class owned_ptr_vector< T, ObjectFactoryClass >;

		public:
			class const_iterator;

			///	STL-compatible random access iterator
			class iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_ptr_vector< T, ObjectFactoryClass >;
				friend class ptr_vector< T, ObjectFactoryClass >;
				friend class ptr_vector< T, ObjectFactoryClass >::const_iterator;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					iterator( typename std::vector< T * >::iterator iter )
					{
						this->iter = iter;
					}

					iterator( const iterator &iter )
					{
						this->iter = iter.iter;
					}

					~iterator( void ) {  }

					iterator &operator=( const iterator &src )
					{
						this->iter = src.iter;
						return *this;
					}

					bool operator==( const iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const iterator &a )
					{
						return this->iter != a.iter;
					}

					T &operator*( void )
					{
						return *( *( this->iter ) );
					}

					T *operator->( void )
					{
						return *( this->iter );
					}

					iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					iterator operator++( int )		//postfix ++
					{
						iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					iterator operator--( int )		//postfix --
					{
						iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					iterator operator+( size_type n )
					{
						iterator tmp( *this );
						tmp += n;
						return tmp;
					}

					iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					iterator operator-( size_type n )
					{
						iterator tmp( *this );
						tmp -= n;
						return tmp;
					}

					difference_type operator-( const iterator &a )
					{
						return this->iter - a.iter;
					}

					T &operator[]( size_type n )
					{
						return *( this->iter[ n ] );
					}

					bool operator<( const iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					iterator( void );

					typename std::vector< T * >::iterator iter;
			};

			///	STL-compatible random access iterator for const data
			class const_iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_ptr_vector< T, ObjectFactoryClass >;
				friend class ptr_vector< T, ObjectFactoryClass >;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					const_iterator( typename std::vector< T * >::const_iterator iter )
					{
						this->iter = iter;
					}

					const_iterator( const const_iterator &citer )
					{
						this->iter = citer.iter;
					}

					const_iterator( const typename ptr_vector< T, ObjectFactoryClass >::iterator &iter )
					{
						this->iter = iter.iter;
					}

					~const_iterator( void ) {  }

					const_iterator &operator=( const const_iterator &src )
					{
						this->iter = src.iter;
					}

					bool operator==( const const_iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const const_iterator &a )
					{
						return this->iter != a.iter;
					}

					const T &operator*( void )
					{
						return ( const T & ) *( *( this->iter ) );
					}

					const T *operator->( void )
					{
						return *( this->iter );
					}

					const_iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					const_iterator operator++( int )	//postfix ++
					{
						const_iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					const_iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					const_iterator operator--( int )	//postfix --
					{
						const_iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					const_iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					const_iterator operator+( size_type n )
					{
						const_iterator tmp( *this );
						tmp += n;
						return tmp;
					}

					const_iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					const_iterator &operator-( size_type n )
					{
						const_iterator tmp( *this );
						tmp -= n;
						return tmp;
					}

					difference_type operator-( const const_iterator &a )
					{
						return this->iter - a.iter;
					}

					const T &operator[]( size_type n )
					{
						return *( this->iter[ n ] );
					}

					bool operator<( const const_iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const const_iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const const_iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const const_iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					const_iterator( void );

					typename std::vector< T * >::const_iterator iter;
			};

			typedef T& reference ;
			typedef const T& const_reference;
			typedef T* pointer;
			typedef const T* const_pointer;
			typedef size_t size_type;
			typedef ptrdiff_t difference_type;
			typedef T value_type;
			typedef std::reverse_iterator< iterator > reverse_iterator;
			typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

			///	Constructs an empty ptr_vector.
			ptr_vector( void ) : v() {  }

			///	Constructs a ptr_vector by copying the pointers stored in another ptr_vector.
			/**	The new ptr_vector will have the same pointers as the old, and both will
				reference the same object instances.
			*/
			ptr_vector( const ptr_vector< T, ObjectFactoryClass > &x ) : v( x.v ) {  }

			///	Constructs a ptr_vector that references the contents of an owned_ptr_vector.
			ptr_vector( const owned_ptr_vector< T, ObjectFactoryClass > &x ) : v( x.v ) {  }

			///	Constructs a ptr_vector from a portion of another ptr_vector, with the interval defined by the supplied iterators.
			ptr_vector( const_iterator first, const_iterator last )
			{
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			///	Constructs a ptr_vector referencing a subset of the objects in an owned_ptr_vector, with the interval defined by the supplied iterators.
			ptr_vector( typename owned_ptr_vector< T, ObjectFactoryClass >::const_iterator first, 
				typename owned_ptr_vector< T, ObjectFactoryClass >::const_iterator last )
			{
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			~ptr_vector( void ) {  }

			///	Copies the stored pointers of one ptr_vector to another.
			/**	Any pointers already stored in the destination ptr_vector are cleared before assignment.
				After assignment, the two ptr_vector instances refer to the same collection of objects.
				However, they are not permanently linked:  Adding or removing objects from one of the
				ptr_vector instances after assignment would not affect the contents of the other.  Rather,
				they are temporarily synchronized.
			*/
			ptr_vector< T, ObjectFactoryClass > &operator=( const ptr_vector< T, ObjectFactoryClass > &x )
			{
				v = x.v;
				return *this;
			}

			///	Copies the stored pointers of an owned_ptr_vector to this ptr_vector.
			/**	Any pointers already stored in the ptr_vector are cleared before assignment.
				After assignment, the ptr_vector and the owned_ptr_vector refer to the same collection of 
				objects.  However, they are not permanently linked:  Adding or removing objects from one of 
				the	collections after assignment would not affect the contents of the other.  Rather,
				they are temporarily synchronized.
			*/
			ptr_vector< T, ObjectFactoryClass > &operator=( const owned_ptr_vector< T, ObjectFactoryClass > &x )
			{
				v = x.v;
				return *this;
			}

			///	Copies a portion of the stored pointers of another ptr_vector to this ptr_vector.
			/**	Any pointers already stored in the destination ptr_vector are cleared before assignment.
			*/
			void assign( const_iterator first, const_iterator last )
			{
				v.clear();
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			///	Copies a portion of the stored pointers of an owned_ptr_vector to this ptr_vector.
			/**	Any pointers already stored in the ptr_vector are cleared before assignment.
			*/
			void assign( typename owned_ptr_vector< T, ObjectFactoryClass >::const_iterator first, 
				typename owned_ptr_vector< T, ObjectFactoryClass >::const_iterator last )
			{
				v.clear();
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			///	Instructs the ptr_vector to allocate storage for at least a certain minimum number of pointers.
			void reserve( size_type n )
			{
				v.reserve( n );
			}

			///	Swaps the pointers stored in two ptr_vectors.
			void swap( ptr_vector< T > &x )
			{
				v.swap( x.v );
			}

			///	Returns an iterator to the first stored object.
			iterator begin( void )
			{
				return iterator( v.begin() );
			}

			///	Returns a const_iterator to the first stored object.
			const_iterator begin( void ) const
			{
				return const_iterator( v.begin() );
			}

			///	Returns an iterator marking the end of the vector.
			iterator end( void )
			{
				return iterator( v.end() );
			}

			///	Returns a const_iterator marking the end of the vector.
			const_iterator end( void ) const
			{
				return const_iterator( v.end() );
			}

			///	Returns a reverse_iterator to the last element, allowing traversal in reverse order.
			reverse_iterator rbegin( void )
			{
				return reverse_iterator( v.rbegin() );
			}

			///	Returns a const_reverse_iterator to the last element, allowing traversal in reverse order.
			const_reverse_iterator rbegin( void ) const
			{
				return const_reverse_iterator( v.rbegin() );
			}

			///	Returns a reverse_iterator marking the end for reverse traversal.
			reverse_iterator rend( void )
			{
				return reverse_iterator( v.rend() );
			}

			///	Returns a const_reverse_iterator marking the end for reverse traversal.
			const_reverse_iterator rend( void ) const
			{
				return const_reverse_iterator( v.rend() );
			}

			///	Returns the number of pointers stored in the ptr_vector.
			size_type size( void ) const
			{
				return v.size();
			}

			///	Returns the maximum number of pointers that can be stored in the ptr_vector.
			size_type max_size( void ) const
			{
				return v.max_size();
			}

			///	Returns the number of pointer slots available without allocating more storage.
			size_type capacity( void ) const
			{
				return v.capacity();
			}

			///	Tests whether or not the ptr_vector is empty.
			bool empty() const
			{
				return v.empty();
			}

			///	Returns a reference to the object at a specific position in the ptr_vector.
			/**	Although the ptr_vector stores pointers to objects, this operator automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference operator[]( size_type n )
			{
				return *( v[ n ] );
			}

			///	Returns a const_reference to the object at a specific position in the ptr_vector.
			/**	Although the ptr_vector stores pointers to objects, this operator automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference operator[]( size_type n ) const
			{
				return *( v[ n ] );
			}

			///	Returns a reference to the object at a specific position in the ptr_vector, after checking the validity of the index.
			/**	Although the ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference at( size_type n )
			{
				return *( v.at( n ) );
			}

			///	Returns a const_reference to the object at a specific position in the ptr_vector, after checking the validity of the index.
			/**	Although the ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference at( size_type n ) const
			{
				return *( v.at( n ) );
			}

			///	Returns the stored pointer at a specific position, after checking the validity of the index.
			T *ptr_at( size_type n )
			{
				return v.at( n );
			}

			///	Returns the stored const pointer (pointer to a const T) at a specific position, after checking the validity of the index.
			const T *ptr_at( size_type n ) const
			{
				return v.at( n );
			}

			///	Returns a reference to the first object in the ptr_vector
			/**	Although the ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference front()
			{
				return *( v.front() );
			}

			///	Returns a const_reference to the first object in the ptr_vector
			/**	Although the ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference front() const
			{
				return *( v.front() );
			}

			///	Returns a reference to the last object in the ptr_vector
			/**	Although the ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference back()
			{
				return *( v.back() );
			}

			///	Returns a const_reference to the last object in the ptr_vector
			/**	Although the ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference back() const
			{
				return *( v.back() );
			}

			///	Adds a pointer to an object to the end of the ptr_vector.
			void push_back( T *x )
			{
				v.push_back( x );
			}

			///	Accepts a reference to an object, and adds a pointer to that object to the end of the ptr_vector.
			void push_back( T &x )
			{
				v.push_back( &x );
			}

			///	Inserts a pointer to an object at a specific position in the ptr_vector.
			iterator insert( iterator position, T *x )
			{
				return iterator( v.insert( position.iter, x ) );
			}

			///	Accepts a reference to an object, and inserts a pointer to that object at a specific position in the ptr_vector.
			iterator insert( iterator position, T &x )
			{
				return iterator( v.insert( position.iter, &x ) );
			}

			///	Copies a group of object pointers from a ptr_vector, inserting them at a specific position in the ptr_vector.
			void insert( iterator position, iterator first, iterator last )
			{
				while( first != last )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Copies a group of object pointers from an owned_ptr_vector, inserting them at a specific position in the ptr_vector.
			void insert( iterator position, typename owned_ptr_vector< T, ObjectFactoryClass >::iterator first, 
				typename owned_ptr_vector< T, ObjectFactoryClass >::iterator last )
			{
				while( first != last )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Replaces a stored pointer at a specific position with a newly-supplied pointer.
			void replace( iterator position, T *x )
			{
				*( position.iter ) = x;
			}

			///	Removes the last stored pointer from the end of the ptr_vector.
			/**	\note	Because the ptr_vector does not own the object, it is <em>not</em> deleted
						when the pointer is removed from the collection.
			*/
			void pop_back( void )
			{
				v.pop_back();
			}

			///	Removes the stored pointer at a specific position in the ptr_vector.
			/**	\note	Because the ptr_vector does not own the object referenced by the pointer, 
						it is <em>not</em> deleted when the pointer is removed from the collection.
			*/
			iterator erase( iterator position )
			{
				return iterator( v.erase( position.iter ) );
			}

			///	Removes the group of stored pointers in the specified range of elements.
			/**	\note	Because the ptr_vector does not own the objects referenced by the pointers, 
						they are <em>not</em> deleted as they are removed from the collection.
			*/
			iterator erase( iterator first, iterator last )
			{
				return iterator( v.erase( first.iter, last.iter ) );
			}

			///	Clears all stored pointers from the ptr_vector.
			/**	\note	Because the ptr_vector does not own the objects referenced by the pointers, 
						they are <em>not</em> deleted as they are removed from the collection.
			*/
			void clear( void )
			{
				v.clear();
			}

		private:
			std::vector< T * > v;

	};

	///	Stores a collection of pointers to objects, and manages those objects' lifetimes.
	/**	The owned_ptr_vector class behaves like the STL std::vector class, but stores
		pointers to objects instead of the objects themselves.  owned_ptr_vector assumes
		ownership of the objects referenced by its pointers, meaning that it takes
		responsibility for their proper deallocation.  The ptr_vector is designed to
		have a similar interface without owning its objects.

		Pointer collections are ideal for storing/referencing polymorphic objects, which
		cannot easily be stored by value.  The owned_ptr_vector can be declared with
		the type parameter set to the type of the base class, and when stored objects
		are accessed, the features of the different derived classes are used rather
		than those of the base classes (compare a std::vector, where adding a derived
		class to a std::vector of the base class causes slicing and data corruption).

		Unlike the ptr_vector, the owned_ptr_vector is neither copyable nor assignable.
		This requirement is imposed due to the unique considerations of object ownership.
		Objects can be transfered between owned_ptr_vector instances, as described
		below, but such an action must be undertaken explicitly by the user.

		The owned_ptr_vector interface is nearly identical to that of the STL std::vector.
		However, the interface is designed so that the normal dereferencing operators
		return references to the objects themselves, rather than the stored pointers to 
		the	objects.  Thus accessing objects stored by reference in a ptr_vector has the 
		same semantics as accessing objects stored by value in std::vector.  A separate 
		set of member functions are provided for obtaining the pointers themselves.
		
		Storing objects by reference requires slightly different semantics for adding
		and removing objects than those of the std::vector, which stores objects by value.
		In particular, objects stored by reference are not necessarily copyable, and
		in general copy semantics would be inappropriate for manipulating such objects.
		Yet at the same time, the references cannot freely be copied to other
		owned_ptr_vectors, since a heap-allocated object can only be owned by one object.
		Likewise, automatic creation and initialization of objects is problematic,
		due to uncertainty about the proper type to create when polymorphic classes
		are in use.

		As a consequence, the part of the std::vector interface that normally creates or
		copies objects has been modified to manage provide member functions with
		appropriate semantics.  The assign member function which would normally copy
		the elements from one vector to another has been split into two member functions,
		one of which takes ownership of unowned objects, and the other of which clones 
		the	objects to produce new heap-allocated objects (see below for the mechanism of
		cloning).  The third form of assign, which would read an input iterator to
		create vector elements, instead is replaced by an assign variant that creates
		new heap-allocated objects from the input interator values.  These three
		functions are assign_takeownership(), assign_clone() and assign_create(),
		respectively.  Similar adjustments have been made for the insert, replace,
		resize and push_back member functions.  
		
		Additionally, a transfer() member function has been added that moves objects 
		between between owned_ptr_vector instances.  Thus a distinction is made between
		the "standard" interface members assign, insert, replace and push_back, which
		have versions that can take ownership of unowned objects, and the transfer()
		members, which moves ownership between two owned_ptr_vector instances.

		The member functions that delete elements from vectors here have the added
		function of deallocating the objects referenced.  A release() member has also
		been added that allows one to obtain ownership of one or more objects, removing
		them from the owned_ptr_vector without destroying them.

		The second template parameter of this class is the ObjectFactoryClass, which
		allows a user to specify an object class to serve as an "object factory."  The
		object factory has an analogous role to the "factory" in the C++ "factory
		pattern" of coding:  It is used to generate new instances of polymorphic
		classes, making a decision about which specific derived class to generate in a
		given case.  The factory is called to generate new objects when initializing
		new elements (as with resize(), assign_create() and insert_create()) and when
		cloning elements from elsewhere (e.g. assign_clone() and insert_clone()).  The
		factory is also used for object deallocation.

		A default object factory class is provided, the DefaultObjectFactory.  This
		factory is designed to be fairly generic, relying on the features of the stored
		class T for most of its work.  It attempts to clone an object of type T by
		creating a new heap-allocated object of type T, passing either a reference to 
		the old T, or a pointer to the old T, to the T constructor.  Thus a class can 
		support cloning in all cases by implementing both a copy constructor and a 
		constructor that accepts a T* and constructs itself according to the contents	
		of the object referenced by the T*.  Unfortunately, this approach is not
		sufficient for cases of polymorphic classes, which would require a custom
		factory class that would know which derived class to create given a T*.

		The object factory is also called upon when attempting to create objects from
		an input stream.  The default factory processes input streams by calling the
		T class constructor once for each stream element, passing the element along.
		The T constructor receives either a copy or a reference depending upon the
		constructor declaration.  This method is fairly generic, although again
		will not work with polymorphic T classes.

		Finally, the object factory manages deallocation.  The default factory simply
		calls the delete operator on the stored T*.

		Of course, the implementation burden to use an owned_ptr_vector depends
		entirely on which features are used.  If normal deletion is sufficient, and
		none of the cloning, creation or input stream features are used, the user
		does not need to take any special action.  But in order to use the cloning
		or creation features, one will need to provide either an appropriate class
		constructor, or a replacement object factory.  Likewise for the input
		stream feature, and for any case where custom deallocation is required.

		To implement a custom object factory, one must supply at least one member,
		a void function called dealloc accepting a T*.  It is assumed that this
		function will know how to delete the object, and after it returns, the
		T* is discarded forever without further processing.  Cloning and creation
		are supported by supplying an overloaded function-call operator accepting
		a const T* to be cloned and returning a T* to the new object.  Input 
		processing requires an overloaded function-call operator accepting a const
		reference to input data of some type X and return a T* to the new object.
		Naturally, the latter would have to be implemented for each type X to be
		processed.

		The object factory is created when an owned_ptr_vector is constructed;
		alternatively the user can supply an object factory on creation, which is
		copied.  The member functions that use the object factory also optionally
		accept a pointer to an object factory instance for one time use.  This
		allows one to supply a special factory to be used for a particular
		purpose at one point in the life of an owned_ptr_vector.

		The documentation below only describes in detail those features which differ
		from the STL std::vector API.  Please see an STL reference for more information
		about the details of the std::vector interface.

		This class is designed for use in conjunction with the ptr_vector class, which
		behaves similarly, but does not own the elements referenced by its pointers.
		In particular, ptr_vector instances are used by this class whenever references 
		to multiple T are involved.  The only special consideration when working with
		ptr_vectors and owned_ptr_vectors is that the ptr_vectors must be declared to
		have the same ObjectFactoryClass type as the owned_ptr_vector.  This is
		required despite the fact that the ptr_vector does not actually use the object
		factory and does not hold an instance of it (the requirement arises from the
		C++ type system).

		\param	T	The type of the object to which the stored pointers point.
		\param	ObjectFactoryClass	(optional) The object factory type to be used.
	*/
	template< typename T, typename ObjectFactoryClass >
	class owned_ptr_vector
	{
		friend class ptr_vector< T, ObjectFactoryClass >;

		public:
			class const_iterator;

			///	STL-compatible random access iterator
			class iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_ptr_vector< T, ObjectFactoryClass >;
				friend class ptr_vector< T, ObjectFactoryClass >;
				friend class owned_ptr_vector< T, ObjectFactoryClass >::const_iterator;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					iterator( typename std::vector< T * >::iterator iter )
					{
						this->iter = iter;
					}

					iterator( const iterator &iter )
					{
						this->iter = iter.iter;
					}

					~iterator( void ) {  }

					iterator &operator=( const iterator &src )
					{
						this->iter = src.iter;
						return *this;
					}

					bool operator==( const iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const iterator &a )
					{
						return this->iter != a.iter;
					}

					T &operator*( void )
					{
						return *( *( this->iter ) );
					}

					T *operator->( void )
					{
						return *( this->iter );
					}

					iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					iterator operator++( int )		//postfix ++
					{
						iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					iterator operator--( int )		//postfix --
					{
						iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					iterator &operator+( size_type n )
					{
						this->iter += n;
						return *this;
					}

					iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					iterator &operator-( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					difference_type operator-( const iterator &a )
					{
						return this->iter - a.iter;
					}

					T &operator[]( size_type n )
					{
						return *( this->iter[ n ] );
					}

					bool operator<( const iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					iterator( void );

					typename std::vector< T * >::iterator iter;
			};

			///	STL-compatible random access iterator for const data
			class const_iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_ptr_vector< T, ObjectFactoryClass >;
				friend class ptr_vector< T, ObjectFactoryClass >;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					const_iterator( typename std::vector< T * >::const_iterator citer )
					{
						this->iter = citer;
					}

					const_iterator( const const_iterator &citer )
					{
						this->iter = citer.iter;
					}

					const_iterator( const typename owned_ptr_vector< T, ObjectFactoryClass >::iterator &iter )
					{
						this->iter = iter.iter;
					}

					~const_iterator( void ) {  }

					const_iterator &operator=( const const_iterator &src )
					{
						this->iter = src.iter;
					}

					bool operator==( const const_iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const const_iterator &a )
					{
						return this->iter != a.iter;
					}

					const T &operator*( void )
					{
						return ( const T & ) *( *( this->iter ) );
					}

					const T *operator->( void )
					{
						return *( this->iter );
					}

					const_iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					const_iterator operator++( int )	//postfix ++
					{
						const_iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					const_iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					const_iterator operator--( int )	//postfix --
					{
						const_iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					const_iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					const_iterator &operator+( size_type n )
					{
						this->iter += n;
						return *this;
					}

					const_iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					const_iterator &operator-( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					difference_type operator-( const const_iterator &a )
					{
						return this->iter - a.iter;
					}

					const T &operator[]( size_type n )
					{
						return *( this->iter[ n ] );
					}

					bool operator<( const const_iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const const_iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const const_iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const const_iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					const_iterator( void );

					typename std::vector< T * >::const_iterator iter;
			};


			typedef T& reference;
			typedef const T& const_reference;
			typedef T* pointer;
			typedef const T* const_pointer;
			typedef size_t size_type;
			typedef ptrdiff_t difference_type;
			typedef T value_type;
			typedef std::reverse_iterator< iterator > reverse_iterator;
			typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

			///	Constructs an empty owned_ptr_vector.
			/**	\param	factory	(optional) An object factory instance from which to construct the resident object factory.
			*/
			owned_ptr_vector( ObjectFactoryClass factory = ObjectFactoryClass() ) : v(), objf( factory ) {  }

			///	Constructs an owned_ptr_vector containing a certain number of elements.
			/**	The new owned_ptr_vector is initialized with n heap-allocated elements.
				If the user supplies a reference to an object of type T, that object
				will be cloned by the object factory to initialize the elements of the
				vector.  Otherwise, the elements will be initialized by cloning the
				default value.  If the default object factory is used, the new objects
				will be created with a call to the new operator, which will invoke the
				T class copy constructor.

				\param	n	The number of elements to initialize
				\param	value	The value to use when initializing the new elements.  If omitted, the default constructor of type T is used.
				\param	factory	(optional) An object factory instance from which to construct the resident object factory.
			*/
			explicit owned_ptr_vector( size_type n, const T& value = T(), ObjectFactoryClass factory = ObjectFactoryClass() ) 
				: v( n ), objf( factory )
			{
				for( size_type i = 0; i < n; i++ )
					v[ n ] = objf( value );
			}

			///	Constructs an owned_ptr_vector from an input stream.
			/**	The input stream is read by dereferencing the InputIterator first,
				processing its value, and then advancing it with the postfix increment
				operator, until first == last.  Each value is processed using the
				object factory.  If the default object factory is used, the new objects
				will be created with a call to the new operator, which will invoke the
				T class constructor that accepts values of the input data type.

				\param	first	An STL-compatible iterator representing the first input data element.
				\param	last	An STL-compatible iterator representing the end of the input data elements.
				\param	factory	(optional) An object factory instance from which to construct the resident object factory.
			*/
			template< typename InputIterator > owned_ptr_vector( InputIterator first, InputIterator last, 
				ObjectFactoryClass factory = ObjectFactoryClass() ) : v(), objf( factory )
			{
				while( first != last )
					v.push_back( objf( *first++ ) );
			}

			///	Constructs an owned_ptr_vector by cloning objects referenced in a ptr_vector.
			/**	All of the objects in the ptr_vector are cloned using the object factory.
				If the default object factory is used, the new objects will be created with a
				call to the new operator, which will invoke the T class copy constructor.

				If you would like the new object_ptr_vector to take control of the objects
				in the original ptr_vector, create an empty owned_ptr_vector and use the
				assign_takeownership() member function.

				\param	x	The ptr_vector whose elements are to be cloned.
				\param	factory	(optional) An object factory instance from which to construct the resident object factory.
			*/
			owned_ptr_vector( const ptr_vector< T, ObjectFactoryClass > &x, ObjectFactoryClass factory = ObjectFactoryClass () ) 
				: objf( factory )
			{
				for( typename ptr_vector< T, ObjectFactoryClass >::const_iterator i = x.begin(); i != x.end(); i++ )
					v.push_back( objf( *i ) );
			}

			///	Constructs an owned_ptr_vector by cloning a subset of objects referenced in an owned_ptr_vector.
			/**	The subset of objects in the owned_ptr_vector referenced in the supplied iterator 
				range are cloned using the object factory.	If the default object factory is 
				used, the new objects will be created with a call to the new operator, which 
				will invoke the T class copy constructor.

				If you would like the new object_ptr_vector to take control of the objects
				in the original owned_ptr_vector, create an empty owned_ptr_vector and use the
				transfer() member function to transfer ownership.

				\param	first	Iterator pointing to the first element to be cloned.
				\param	last	Iterator pointing one unit past the last element to be cloned.
				\param	factory	(optional) An object factory instance from which to construct the resident object factory.
			*/
			owned_ptr_vector( const_iterator first, const_iterator last, ObjectFactoryClass factory = ObjectFactoryClass () )
				: v(), objf( factory )
			{
				while( first != last )
					v.push_back( objf( &*first++ ) );
			}

			///	Constructs an owned_ptr_vector by cloning a subset of objects referenced in a ptr_vector.
			/**	The subset of objects in the ptr_vector referenced in the supplied iterator 
				range are cloned using the object factory.	If the default object factory is 
				used, the new objects will be created with a call to the new operator, which 
				will invoke the T class copy constructor.

				If you would like the new owned_ptr_vector to take control of the objects
				in the original ptr_vector, create an empty owned_ptr_vector and use the
				assign_takeownership() member function.

				\param	first	Iterator pointing to the first element to be cloned.
				\param	last	Iterator pointing one unit past the last element to be cloned.
				\param	factory	(optional) An object factory instance from which to construct the resident object factory.
			*/
			owned_ptr_vector( typename ptr_vector< T, ObjectFactoryClass >::const_iterator first, 
				typename ptr_vector< T, ObjectFactoryClass >::const_iterator last, 
				ObjectFactoryClass factory = ObjectFactoryClass () ) : v(), objf( factory )
			{
				while( first != last )
					v.push_back( objf( &*first++ ) );
			}

			///	Constructs an owned_ptr_vector taking ownership of the objects in another owned_ptr_vector.
			/**	This member function is intended to support the safe transfer of a collection of
				objects between two owned_ptr_vector instances, where the transfer() member
				function is not feasible.  Although this interface is somewhat cumbersome, it
				ensures exception safety under all circumstances.

				The design of this mechanism was intended to support circumstances where multiple 
				objects	need to pass between an owned_ptr_vector in one scope and a newly
				created owned_ptr_vector in another scope, without the two having direct
				contact.  This could occur when a function A needs to pass a collection of
				objects to a function B that it calls, without passing along direct access
				to A's owned_ptr_vector (e.g. if the starting owned_ptr_vector is a private
				member of a data structure that B is restricted from accessing).  The
				release_safe() member function is used to obtain the exception-safe
				collection from the origin container, which is then passed along to the
				destination.

				The mechanism of exchange is a temporary heap-allocated owned_ptr_vector held
				in a std::auto_ptr.  The std::auto_ptr wrapper ensures that the heap-allocated
				owned_ptr_vector is automatically deleted after the exchange, while the use of
				an owned_ptr_vector as the medium for exchange ensures that the objects
				being transfered are not leaked if the exchange falls through.

				If exception safety is not important, the objects can be transferred via
				stack-allocated ptr_vector instances instead.  The first is obtained via the
				release() member function, which returns a ptr_vector.  This can be copied as
				needed to complete the exchange.  This method is not generally recommended,
				however.

				\param	invector	The collection of objects to take ownership of.
				\param	factory	(optional) An object factory instance from which to construct the resident object factory.
			*/
			owned_ptr_vector( std::auto_ptr< owned_ptr_vector< T, ObjectFactoryClass > > &invector,
				ObjectFactoryClass factory = ObjectFactoryClass() ) : v(), objf( factory )
			{
				this->swap( *invector );
			}

			~owned_ptr_vector( void )
			{
				for( typename std::vector< T * >::iterator i = v.begin(); i != v.end(); i++ )
					objf.dealloc( *i );
			}

			///	Clears the contents of the container, and clones a subset of objects referenced in an owned_ptr_vector.
			/**	The subset of objects in the owned_ptr_vector referenced in the supplied iterator 
				range are cloned using the object factory.	If the default object factory is 
				used, the new objects will be created with a call to the new operator, which 
				will invoke the T class copy constructor.

				If you would like the object_ptr_vector to take control of the objects
				in the original owned_ptr_vector, rather than to clone them, use the
				transfer() member function to transfer ownership.

				Any objects referenced previously by the owned_ptr_vector are deallocated.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used for cloning.  The resident factory
				would not be affected.

				\param	first	Iterator pointing to the first element to be cloned.
				\param	last	Iterator pointing one unit past the last element to be cloned.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void assign_clone( const_iterator first, const_iterator last, ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				this->clear();
				while( first != last )
					v.push_back( ( *factory )( &*first++ ) );
			}

			///	Clears the contents of the container, and clones a subset of objects referenced in a ptr_vector.
			/**	The subset of objects in the ptr_vector referenced in the supplied iterator 
				range are cloned using the object factory.	If the default object factory is 
				used, the new objects will be created with a call to the new operator, which 
				will invoke the T class copy constructor.

				If you would like the object_ptr_vector to take control of the objects
				in the original ptr_vector, rather than to clone them, use the
				assign_takeownership() member function to transfer ownership.

				Any objects referenced previously by the owned_ptr_vector are deallocated.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used for cloning.  The resident factory
				would not be affected.

				\param	first	Iterator pointing to the first element to be cloned.
				\param	last	Iterator pointing one unit past the last element to be cloned.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void assign_clone( typename ptr_vector< T, ObjectFactoryClass >::const_iterator first, 
				typename ptr_vector< T, ObjectFactoryClass >::const_iterator last, ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				this->clear();
				while( first != last )
					v.push_back( ( *factory )( &*first++ ) );
			}
			///	Clears the contents of the container, and creates a certain number of new elements.
			/**	The owned_ptr_vector is filled with n heap-allocated elements.
				If the user supplies a reference to an object of type T, that object
				will be cloned by the object factory to initialize the elements of the
				vector.  Otherwise, the elements will be initialized by cloning the
				default value.  If the default object factory is used, the new objects
				will be created with a call to the new operator, which will invoke the
				T class copy constructor.

				Any objects referenced previously by the owned_ptr_vector are deallocated.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used for creating new objects.  The resident factory
				would not be affected.

				\param	n	The number of elements to initialize
				\param	value	The value to use when initializing the new elements.  If omitted, the default constructor of type T is used.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void assign_create( size_type n, const T& value = T(), ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				this->clear();
				for( size_type i = 0; i < n; i++ )
					v.push_back( ( *factory )( &value ) );
			}


			///	Clears the contents of the container, and processes an input stream to create a new set of objects.
			/**	The input stream is read by dereferencing the InputIterator first,
				processing its value, and then advancing it with the postfix increment
				operator, until first == last.  Each value is processed using the
				object factory.  If the default object factory is used, the new objects
				will be created with a call to the new operator, which will invoke the
				T class constructor that accepts values of the input data type.

				Any objects referenced previously by the owned_ptr_vector are deallocated.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used to process the input stream.  The resident factory
				would not be affected.

				\param	first	An STL-compatible iterator representing the first input data element.
				\param	last	An STL-compatible iterator representing the end of the input data elements.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			template< typename InputIterator > 
			void assign_create( InputIterator first, InputIterator last, ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				this->clear();
				while( first != last )
					v.push_back( ( *factory )( *first++ ) );
			}

			///	Clears the contents of the container, and takes ownership of a subset of objects referenced in a ptr_vector.
			/**	If you would like to clone the objects in the original ptr_vector, 
				rather than to take ownership of them, use the assign_clone()
				member function.

				\warning	Be certain that it is safe for the owned_ptr_vector to
							assume ownership of the objects before adding.  Do NOT add
							stack-allocated objects to the collection.

				Any objects referenced previously by the owned_ptr_vector are deallocated.

				\param	first	Iterator pointing to the first element to be owned.
				\param	last	Iterator pointing one unit past the last element to be owned.
			*/
			void assign_takeownership( typename ptr_vector< T, ObjectFactoryClass >::const_iterator first, 
				typename ptr_vector< T, ObjectFactoryClass >::const_iterator last )
			{
				while( first != last )
					v.push_back( &*first++ );
			}

			///	Clears the contents of the container, and takes ownership of the objects referenced in a ptr_vector.
			/**	If you would like to clone the objects in the original ptr_vector, 
				rather than to take ownership of them, use the assign_clone()
				member function.

				\warning	Be certain that it is safe for the owned_ptr_vector to
							assume ownership of the objects before adding.  Do NOT add
							stack-allocated objects to the collection.

				Any objects referenced previously by the owned_ptr_vector are deallocated.

				\param	container	The ptr_vector of objects to be added.
			*/
			void assign_takeownership( ptr_vector< T, ObjectFactoryClass > &container )
			{
				this->clear();
				v.swap( container.v );
			}

			///	Clears the contents of the container, and takes ownership of the objects in another owned_ptr_vector.
			/**	This member function is intended to support the safe transfer of a collection of
				objects between two owned_ptr_vector instances, where the transfer() member
				function is not feasible.  Although this interface is somewhat cumbersome, it
				ensures exception safety under all circumstances.

				The design of this mechanism was intended to support circumstances where multiple 
				objects	need to pass between an owned_ptr_vector in one scope and a newly
				created owned_ptr_vector in another scope, without the two having direct
				contact.  This could occur when a function A needs to return a collection of
				objects to a function B that called it, without having direct access
				to B's owned_ptr_vector (e.g. if the starting owned_ptr_vector is a private
				member of a data structure that A is restricted from accessing).  The
				release_safe() member function is used to obtain the exception-safe
				collection from the origin container, which is then passed along to the
				destination.

				The mechanism of exchange is a temporary heap-allocated owned_ptr_vector held
				in a std::auto_ptr.  The std::auto_ptr wrapper ensures that the heap-allocated
				owned_ptr_vector is automatically deleted after the exchange, while the use of
				an owned_ptr_vector as the medium for exchange ensures that the objects
				being transfered are not leaked if the exchange falls through.

				If exception safety is not important, the objects can be transferred via
				stack-allocated ptr_vector instances instead.  The first is obtained via the
				release() member function, which returns a ptr_vector.  This can be copied as
				needed to complete the exchange.  This method is not generally recommended,
				however.

				Any objects referenced previously by the owned_ptr_vector are deallocated.

				\param	invector	The collection of objects to take ownership of.
			*/
			void assign_takeownership( std::auto_ptr< owned_ptr_vector< T, ObjectFactoryClass > > &invector )
			{
				this->clear();
				v.swap( invector->v );
			}

			///	Returns the number of objects stored in the owned_ptr_vector.
			size_type size( void ) const
			{
				return v.size();
			}

			///	Returns the maximum number of pointers that can be stored in the ptr_vector.
			size_type max_size( void ) const
			{
				return v.max_size();
			}

			///	Instructs the owned_ptr_vector to allocate storage for at least a certain minimum number of pointers.
			void reserve( size_type n )
			{
				v.reserve( n );
			}

			///	Resizes the container, deallocating objects or creating new objects as needed.
			/**	If the new size is less than the current size, objects at the end of the
				vector are deallocated until the desired size is reached.

				If the new size is greater than the current size, new heap-allocated
				objects are created and appended to the collection using the object
				factory.  If the user supplies a reference to an object of type T, that object
				will be cloned by the object factory to initialize the elements of the
				vector.  Otherwise, the elements will be initialized by cloning the
				default value.  If the default object factory is used, the new objects
				will be created with a call to the new operator, which will invoke the
				T class copy constructor.

				\param	sz	The new size of the container.
				\param	value	The value to use when initializing new elements.  If omitted, the default constructor of type T is used.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void resize( size_type sz, T &value = T(), ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				if( sz < v.size() )
				{
					while( v.size() > sz )
					{
						objf.dealloc( v.back() );
						v.pop_back();
					}
				}
				else if( sz > v.size() )
				{
					while( v.size() < sz )
						v.push_back( ( *factory )( &value ) );
				}
			}

			///	Returns the number of pointer slots available without allocating more storage.
			size_type capacity( void ) const
			{
				return v.capacity();
			}

			///	Tests whether or not the owned_ptr_vector is empty.
			bool empty() const
			{
				return v.empty();
			}

			///	Swaps the pointers in two owned_ptr_vectors.
			void swap( owned_ptr_vector< T, ObjectFactoryClass > &x )
			{
				v.swap( x.v );
			}

			///	Returns an iterator to the first stored object.
			iterator begin( void )
			{
				return iterator( v.begin() );
			}

			///	Returns a const_iterator to the first stored object.
			const_iterator begin( void ) const
			{
				return const_iterator( v.begin() );
			}

			///	Returns an iterator marking the end of the vector.
			iterator end( void )
			{
				return iterator( v.end() );
			}

			///	Returns a const_iterator marking the end of the vector.
			const_iterator end( void ) const
			{
				return const_iterator( v.end() );
			}

			///	Returns a reverse_iterator to the last element, allowing traversal in reverse order.
			reverse_iterator rbegin( void )
			{
				return reverse_iterator( v.rbegin() );
			}

			///	Returns a const_reverse_iterator to the last element, allowing traversal in reverse order.
			const_reverse_iterator rbegin( void ) const
			{
				return const_reverse_iterator( v.rbegin() );
			}

			///	Returns a reverse_iterator marking the end for reverse traversal.
			reverse_iterator rend( void )
			{
				return reverse_iterator( v.rend() );
			}

			///	Returns a const_reverse_iterator marking the end for reverse traversal.
			const_reverse_iterator rend( void ) const
			{
				return const_reverse_iterator( v.rend() );
			}

			///	Returns a reference to the object at a specific position in the owned_ptr_vector.
			/**	Although the owned_ptr_vector stores pointers to objects, this operator automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference operator[]( size_type n )
			{
				return *( v[ n ] );
			}

			///	Returns a const_reference to the object at a specific position in the owned_ptr_vector.
			/**	Although the owned_ptr_vector stores pointers to objects, this operator automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference operator[]( size_type n ) const
			{
				return *( v[ n ] );
			}

			///	Returns a reference to the object at a specific position in the owned_ptr_vector, after checking the validity of the index..
			/**	Although the owned_ptr_vector stores pointers to objects, this operator automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference at( size_type n )
			{
				return *( v.at( n ) );
			}

			///	Returns a reference to the object at a specific position in the owned_ptr_vector, after checking the validity of the index..
			/**	Although the owned_ptr_vector stores pointers to objects, this operator automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference at( size_type n ) const
			{
				return *( v.at( n ) );
			}

			///	Returns the stored pointer at a specific position, after checking the validity of the index.
			T *ptr_at( size_type n )
			{
				return v.at( n );
			}

			///	Returns the stored const pointer (pointer to a const T) at a specific position, after checking the validity of the index.
			const T *ptr_at( size_type n ) const
			{
				return v.at( n );
			}

			///	Returns a reference to the first object in the owned_ptr_vector.
			/**	Although the owned_ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference front()
			{
				return *( v.front() );
			}

			///	Returns a const_reference to the first object in the owned_ptr_vector.
			/**	Although the owned_ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference front() const
			{
				return *( v.front() );
			}

			///	Returns a reference to the last object in the owned_ptr_vector.
			/**	Although the owned_ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			reference back()
			{
				return *( v.back() );
			}

			///	Returns a const_reference to the last object in the owned_ptr_vector.
			/**	Although the owned_ptr_vector stores pointers to objects, this member function automatically
				dereferences the stored pointer, returning a reference to the object itself.  To
				obtain the raw pointer, use the ptr_at() member.
			*/
			const_reference back() const
			{
				return *( v.back() );
			}

			///	Adds an object to the end of the collection.
			/**	\warning	Be certain that it is safe for the owned_ptr_vector to
							assume ownership of the object before adding.  Do NOT add
							stack-allocated objects to the collection.
			*/
			void push_back( T *x )
			{
				v.push_back( x );
			}

			///	Adds an object to the end of the collection.
			/**	Ownership is transferred from the stack_ptr, and the stack_ptr is cleared.
			*/
			void push_back( stack_ptr< T > &x )
			{
				v.push_back( x.release() );
			}

			///	Adds an object to the end of the collection.
			/**	If clone is set to true, the supplied object will be cloned using the
				object factory.  Otherwise, the object itself will be added to the
				container.

				If the default object factory is used for cloning, the new objects 
				will be created with a call to the new operator, which will invoke the 
				T class copy constructor.
				
				If a pointer to an object factory instance is supplied, that particular
				instance will be used for creating new objects.  The resident factory
				would not be affected.

				\warning	If clone is set to false, be certain that it is 
							safe for the owned_ptr_vector to assume ownership 
							of the object before adding.  Do NOT add
							stack-allocated objects to the collection.

				\param	x	A reference to the object to clone or add.
				\param	clone	Indicates whether a clone of the object, or the object itself, should be added.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void push_back( const T& x, bool clone = true, ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				if( !clone ) v.push_back( &x );
				else v.push_back( ( *factory )( &x ) );
			}

			///	Inserts an object into the collection at a specified position.
			/**	\warning	Be certain that it is safe for the owned_ptr_vector to
							assume ownership of the object before adding.  Do NOT add
							stack-allocated objects to the collection.
			*/
			iterator insert( iterator position, T *x )
			{
				return iterator( v.insert( position.iter, x ) );
			}

			///	Inserts a clone of an object into the collection at a specified position.
			/**	The supplied object will be cloned using the object factory.  If the 
				default object factory is used for cloning, the new objects will be 
				created with a call to the new operator, which will invoke the T class 
				copy constructor.
				
				If a pointer to an object factory instance is supplied, that particular
				instance will be used for cloning.  The resident factory
				would not be affected.

				\param	position	The position where the object is to be inserted.
				\param	x	A reference to the object to clone.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			iterator insert_clone( iterator position, T& x, ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				return iterator( v.insert( position.iter, ( *factory )( &x ) ) );
			}

			///	Inserts clones of a range of objects from another owned_ptr_vector into the collection at a specified position.
			/**	The subset of objects in the owned_ptr_vector referenced in the supplied iterator 
				range are cloned using the object factory.	If the default object factory is 
				used, the new objects will be created with a call to the new operator, which 
				will invoke the T class copy constructor.

				If you would like the object_ptr_vector to take control of the objects
				in the original owned_ptr_vector, rather than to clone them, use the
				transfer() member function to transfer ownership.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used for cloning.  The resident factory
				would not be affected.

				\param	position	The position where the objects are to be inserted.
				\param	first	Iterator pointing to the first element to be cloned.
				\param	last	Iterator pointing one unit past the last element to be cloned.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void insert_clone( iterator position, const_iterator first, const_iterator last, ObjectFactoryClass *factory = 0 )
			{
				while( first != last )
				{
					v.insert( position.iter, ( *factory )( &*first ) );
					position++;
					first++;
				}
			}

			///	Clones a subset of objects referenced in a ptr_vector, and inserts them into the collection at a specified position.
			/**	The subset of objects in the ptr_vector referenced in the supplied iterator 
				range are cloned using the object factory.	If the default object factory is 
				used, the new objects will be created with a call to the new operator, which 
				will invoke the T class copy constructor.

				If you would like the object_ptr_vector to take control of the objects
				in the original ptr_vector, rather than to clone them, use the
				insert_takeownership() member function to transfer ownership.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used for cloning.  The resident factory
				would not be affected.

				\param	position	The position where the objects are to be inserted.
				\param	first	Iterator pointing to the first element to be cloned.
				\param	last	Iterator pointing one unit past the last element to be cloned.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void insert_clone( iterator position, typename ptr_vector< T, ObjectFactoryClass >::const_iterator first, 
				typename ptr_vector< T, ObjectFactoryClass >::const_iterator last, 
				ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				while( first != last )
				{
					position = iterator( v.insert( position.iter, ( *factory )( &*first ) ) );
					position++;
					first++;
				}
			}

			///	Inserts a number of new elements into the collection at a specified position.
			/**	This function creates and inserts n heap-allocated elements.
				If the user supplies a reference to an object of type T, that object
				will be cloned by the object factory to initialize the elements of the
				vector.  Otherwise, the elements will be initialized by cloning the
				default value.  If the default object factory is used, the new objects
				will be created with a call to the new operator, which will invoke the
				T class copy constructor.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used for creating new objects.  The resident factory
				would not be affected.

				\param	position	The position where the objects are to be inserted.
				\param	n	The number of elements to initialize
				\param	x	The value to use when initializing the new elements.  If omitted, the default constructor of type T is used.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void insert_create( iterator position, size_type n, T& x = T(), ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				for( size_type i = 0; i < n; i++ )
				{
					position = iterator( v.insert( position.iter, ( *factory )( &x ) ) );
					position++;
				}
			}

			///	Processes an input stream to create a new set of objects, and inserts those objects into the collection at the specified position.
			/**	The input stream is read by dereferencing the InputIterator first,
				processing its value, and then advancing it with the postfix increment
				operator, until first == last.  Each value is processed using the
				object factory.  If the default object factory is used, the new objects
				will be created with a call to the new operator, which will invoke the
				T class constructor that accepts values of the input data type.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used to process the input stream.  The resident factory
				would not be affected.

				\param	position	The position where the objects are to be inserted.
				\param	first	An STL-compatible iterator representing the first input data element.
				\param	last	An STL-compatible iterator representing the end of the input data elements.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			template< typename InputIterator > void insert_create( iterator position, InputIterator first, InputIterator last,
				ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				while( first != last )
				{
					v.insert( position.iter, ( *factory )( *first ) );
					position++;
					first++;
				}
			}

			///	Takes ownership of a subset of objects referenced in a ptr_vector, and inserts them into the collection at a specified position.
			/**	If you would like to clone the objects in the original ptr_vector, 
				rather than to take ownership of them, use the insert_clone()
				member function.

				\warning	Be certain that it is safe for the owned_ptr_vector to
							assume ownership of the objects before adding.  Do NOT add
							stack-allocated objects to the collection.

				\param	position	The position where the objects are to be inserted.
				\param	first	Iterator pointing to the first element to be owned.
				\param	last	Iterator pointing one unit past the last element to be owned.
			*/
			void insert_takeownership( iterator position, typename ptr_vector< T, ObjectFactoryClass >::const_iterator first, 
				typename ptr_vector< T >::const_iterator last )
			{
				while( first != last )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Takes ownership of an object, and inserts it into the collection at a specified position.
			/**	\warning	Be certain that it is safe for the 
							owned_ptr_vector to assume ownership 
							of the object before adding.  Do NOT add
							stack-allocated objects to the collection.
			*/
			iterator insert_takeownership( iterator position, T& x )
			{
				return iterator( v.insert( position.iter, &x ) );
			}

			///	Takes ownership of the objects referenced in a ptr_vector, and inserts them into the collection at a specified position.
			/**	If you would like to clone the objects in the original ptr_vector, 
				rather than to take ownership of them, use the insert_clone()
				member function.

				\warning	Be certain that it is safe for the owned_ptr_vector to
							assume ownership of the objects before adding.  Do NOT add
							stack-allocated objects to the collection.

				\param	position	The position where the objects are to be inserted.
				\param	container	The ptr_vector of objects to be added.
			*/
			void insert_takeownership( iterator position, ptr_vector< T, ObjectFactoryClass > &container )
			{
				typename ptr_vector< T, ObjectFactoryClass >::iterator first = container.begin();
				while( first != container.end() )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Takes ownership of the objects in another owned_ptr_vector, and inserts them in the collection at a specified position.
			/**	This member function is intended to support the safe transfer of a collection of
				objects between two owned_ptr_vector instances, where the transfer() member
				function is not feasible.  Although this interface is somewhat cumbersome, it
				ensures exception safety under all circumstances.

				The design of this mechanism was intended to support circumstances where multiple 
				objects	need to pass between an owned_ptr_vector in one scope and a newly
				created owned_ptr_vector in another scope, without the two having direct
				contact.  This could occur when a function A needs to return a collection of
				objects to a function B that called it, without having direct access
				to B's owned_ptr_vector (e.g. if the starting owned_ptr_vector is a private
				member of a data structure that A is restricted from accessing).  The
				release_safe() member function is used to obtain the exception-safe
				collection from the origin container, which is then passed along to the
				destination.

				The mechanism of exchange is a temporary heap-allocated owned_ptr_vector held
				in a std::auto_ptr.  The std::auto_ptr wrapper ensures that the heap-allocated
				owned_ptr_vector is automatically deleted after the exchange, while the use of
				an owned_ptr_vector as the medium for exchange ensures that the objects
				being transfered are not leaked if the exchange falls through.

				If exception safety is not important, the objects can be transferred via
				stack-allocated ptr_vector instances instead.  The first is obtained via the
				release() member function, which returns a ptr_vector.  This can be copied as
				needed to complete the exchange.  This method is not generally recommended,
				however.

				\param	position	The position where the objects are to be inserted.
				\param	invector	The collection of objects to take ownership of.
			*/
			void insert_takeownership( iterator position, std::auto_ptr< owned_ptr_vector< T, ObjectFactoryClass > > &invector )
			{
				this->transfer( position, *invector );
			}

			///	Transfers objects from another owned_ptr_vector, and inserts them at the specified position.
			/**	All objects owned by the source collection are transferred.

				To clone the objects instead of transferring them, use the assign_clone() or
				insert_clone() member functions.

				To transfer objects between owned_ptr_vector instances when it is not possible to
				access the source and destination instances simultaneously (i.e. if they are in
				different scopes, or one/both are protected members of a class), use the
				release_safe() and either the assign_takeownership() or insert_takeownership()
				member functions.

				\param	position	The position where the objects are to be inserted.
				\param	prev_owner	The collection of objects to take ownership of.
			*/
			void transfer( iterator position, owned_ptr_vector< T, ObjectFactoryClass > &prev_owner )
			{
				ptr_vector< T > newvector = prev_owner.release();
				this->insert_takeownership( position, newvector.begin(), newvector.end() );
			}

			///	Transfers a specific object from another owned_ptr_vector, and inserts it at the specified position.
			/**	Only the specific object referenced by the second parameters "obj" is transferred.

				To clone the object instead of transferring it, use the assign_clone() or
				insert_clone() member functions.

				To transfer an object between owned_ptr_vector instances when it is not possible to
				access the source and destination instances simultaneously (i.e. if they are in
				different scopes, or one/both are protected members of a class), use the
				release_safe() and either the assign_takeownership() or insert_takeownership()
				member functions.

				\param	position	The position where the objects are to be inserted.
				\param	obj			An iterator pointing to the object to be transferred.
				\param	prev_owner	The collection of objects that is the source of the transfer.
			*/
			void transfer( iterator position, iterator obj, owned_ptr_vector< T, ObjectFactoryClass > &prev_owner )
			{
				v.insert( position.iter, obj.get_ptr() );
				prev_owner.release( obj );
			}

			///	Transfers objects from another owned_ptr_vector, and inserts them at the specified position.
			/**	The group of objects specified by the iterator range are transferred.

				To clone the objects instead of transferring them, use the assign_clone() or
				insert_clone() member functions.

				To transfer objects between owned_ptr_vector instances when it is not possible to
				access the source and destination instances simultaneously (i.e. if they are in
				different scopes, or one/both are protected members of a class), use the
				release_safe() and either the assign_takeownership() or insert_takeownership()
				member functions.

				\param	position	The position where the objects are to be inserted.
				\param	first		An iterator pointing to the first object to be transferred.
				\param	last		An iterator pointing one element past the last object to be transferred.
				\param	prev_owner	The collection of objects that is the source of the transfer.
			*/
			void transfer( iterator position, iterator first, iterator last, owned_ptr_vector< T, ObjectFactoryClass > &prev_owner )
			{
				iterator first2( first );
				while( first2 != last )
				{
					position = iterator( v.insert( position.iter, first2.get_ptr() ) );
					position++;
					first2++;
				}
				prev_owner.release( first, last );
			}

			///	Replaces the object at a specified position with another object.
			/**	The object previously at the specified position is deallocated.

				\warning	Be certain that it is safe for the owned_ptr_vector to
							assume ownership of the object before adding.  Do NOT add
							stack-allocated objects to the collection.
				*/
			void replace( iterator position, T *x )
			{
				objf.dealloc( *( position.iter ) );
				*( position.iter ) = x;
			}

			///	Replaces the object at a specified position with a newly created object.
			/**	The object previously at the specified position is deallocated.
				
				If the user supplies a reference to an object of type T, that object
				will be cloned by the object factory to initialize the replacement 
				element.  Otherwise, the element will be initialized by cloning the
				default value.  If the default object factory is used, the new object
				will be created with a call to the new operator, which will invoke the
				T class copy constructor.

				If a pointer to an object factory instance is supplied, that particular
				instance will be used for creating new objects.  The resident factory
				would not be affected.

				\param	position	Iterator pointing to the object to be replaced.
				\param	value	The value to use when initializing the new elements  If omitted, the default constructor of type T is used.
				\param	factory	(optional) An object factory instance to use for the duration of this call.
			*/
			void replace_create( iterator position, T &value, ObjectFactoryClass *factory = 0 )
			{
				if( !factory ) factory = &objf;

				objf.dealloc( *( position.iter ) );
				*( position.iter ) = factory( &value );
			}

			///	Replaces the object at a specified position with another object.
			/**	\warning	Be certain that it is safe for the 
							owned_ptr_vector to assume ownership 
							of the object before adding.  Do NOT add
							stack-allocated objects to the collection.
			*/
			void replace_takeownership( iterator position, T &value )
			{
				objf.dealloc( *( position.iter ) );
				*( position.iter ) = &value;
			}

			///	Removes and deallocates the last element of the collection.
			void pop_back( void )
			{
				objf.dealloc( v.back() );
				v.pop_back();
			}

			///	Remove and deallocate all objects stored in the owned_ptr_vector.
			void clear( void )
			{
				for( typename std::vector< T * >::iterator i = v.begin(); i < v.end(); i++ )
					objf.dealloc( *i );
				v.clear();
			}

			///	Removes and deallocates the object at the specified position in the collection.
			iterator erase( iterator position )
			{
				objf.dealloc( position.get_ptr() );
				return iterator( v.erase( position.iter ) );
			}

			///	Removes and deallocates a specified range of objects from the collection.
			iterator erase( iterator first, iterator last )
			{
				iterator first2 = first;
				while( first2 != last )
				{
					objf.dealloc( first2.get_ptr() );
					first2++;
				}
				return iterator( v.erase( first.iter, last.iter ) );
			}

			///	Releases ownership of the object at the specified position in the collection.
			/**	The object is removed from the collection, and the pointer to the object
				is returned.
				
				\warning	Using this member function to transfer ownership may not be
							exception-safe.  The release_safe() function is preferred.
			*/
			T *release( iterator position )
			{
				T *tmp = position.get_ptr();
				v.erase( position.iter );
				return tmp;
			}

			///	Releases ownership of a specified range of objects from the collection.
			/**	The objects are removed from the collection, and a ptr_vector is
				returned referencing the objects.
				
				\warning	Using this member function to transfer ownership may not be
							exception-safe.  The release_safe() function is preferred.
			*/
			ptr_vector< T, ObjectFactoryClass > release( iterator first, iterator last )
			{
				ptr_vector< T, ObjectFactoryClass > newvector( first, last );
				v.erase( first.iter, last.iter );
				return newvector;
			}

			///	Releases ownership of all the objects in the collection.
			/**	The objects are removed from the collection, and a ptr_vector is
				returned referencing the objects.
				
				\warning	Using this member function to transfer ownership may not be
							exception-safe.  The release_safe() function is preferred.
			*/
			ptr_vector< T, ObjectFactoryClass > release( void )
			{
				ptr_vector< T, ObjectFactoryClass > newvector( *this );
				v.clear();
				return newvector;
			}

			///	Releases ownership of the object at the specified position in the collection.
			/**	The object is removed from the collection, and the pointer to the object
				is returned.  The pointer is wrapped in a std::auto_ptr to ensure exception
				safety.
			*/
			std::auto_ptr< T > release_safe( iterator position )
			{
				std::auto_ptr< T > tmp = position.get_ptr();
				v.erase( position.iter );
				return tmp;
			}

			///	Releases ownership of a specified range of objects from the collection.
			/**	This function is part of the exception-safe mechanism for transferring
				objects between owned_ptr_vector instances, where the transfer() member
				function cannot be used (i.e. when the two instances are in different scopes,
				or when one/both instances are protected members of classes).  The objects 
				are removed from the collection, and a special heap-allocated owned_ptr_vector
				is returned, wrapped in a std::auto_ptr, to contain the objects.  The
				std::auto_ptr can be passed by value between functions, preserving the
				collection of heap-allocated objects.  Eventually these can be added
				to another owned_ptr_vector using the constructor, assign_takeownership()
				or insert_takeownership() member functions.
			*/
			std::auto_ptr< owned_ptr_vector< T, ObjectFactoryClass > > release_safe( iterator first, iterator last )
			{
				std::auto_ptr< owned_ptr_vector< T, ObjectFactoryClass > > newvector( new owned_ptr_vector< T, ObjectFactoryClass >() );
				newvector->transfer( newvector->begin(), first, last, *this );
				return newvector;
			}

			///	Releases ownership of all the objects in the collection.
			/**	This function is part of the exception-safe mechanism for transferring
				objects between owned_ptr_vector instances, where the transfer() member
				function cannot be used (i.e. when the two instances are in different scopes,
				or when one/both instances are protected members of classes).  The objects 
				are removed from the collection, and a special heap-allocated owned_ptr_vector
				is returned, wrapped in a std::auto_ptr, to contain the objects.  The
				std::auto_ptr can be passed by value between functions, preserving the
				collection of heap-allocated objects.  Eventually these can be added
				to another owned_ptr_vector using the constructor, assign_takeownership()
				or insert_takeownership() member functions.
			*/
			std::auto_ptr< owned_ptr_vector< T, ObjectFactoryClass > > release_safe( void )
			{
				std::auto_ptr< owned_ptr_vector< T, ObjectFactoryClass > > newvector( new owned_ptr_vector< T, ObjectFactoryClass >() );
				newvector->swap( *this );
				return newvector;
			}

			///	Returns a reference to the resident object factory instance.
			ObjectFactoryClass &get_factory( void )
			{
				return objf;
			}

			///	Replaces the resident object factory with a new one.
			/**	Copy semantics are used to duplicate the newly-supplied factory and
				store a copy as the new resident factory.
			*/
			void set_factory( ObjectFactoryClass factory )
			{
				this->objf = factory;
			}

		private:
			owned_ptr_vector( const owned_ptr_vector< T, ObjectFactoryClass > &x );
			owned_ptr_vector< T, ObjectFactoryClass > &operator=( const ptr_vector< T, ObjectFactoryClass > &x );
			owned_ptr_vector< T, ObjectFactoryClass > &operator=( const owned_ptr_vector< T, ObjectFactoryClass > &x );

			std::vector< T * > v;
			ObjectFactoryClass objf;

			//	special proxy iterators for standard algorithms?
	};

	
	template< typename T, typename ObjectFactoryClass >
	bool operator==( ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		if( lhs.size() != rhs.size() ) return false;
		return std::equal( lhs.begin(), lhs.end(), rhs.begin() );
	}
	
	template< typename T, typename ObjectFactoryClass >
	bool operator==( owned_ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		if( lhs.size() != rhs.size() ) return false;
		return std::equal( lhs.begin(), lhs.end(), rhs.begin() );
	}
	
	template< typename T, typename ObjectFactoryClass >
	bool operator==( ptr_vector< T, ObjectFactoryClass > &lhs, owned_ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		if( lhs.size() != rhs.size() ) return false;
		return std::equal( lhs.begin(), lhs.end(), rhs.begin() );
	}
	
	template< typename T, typename ObjectFactoryClass >
	bool operator!=( ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return !( lhs == rhs );
	}
	
	template< typename T, typename ObjectFactoryClass >
	bool operator!=( owned_ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return !( lhs == rhs );
	}
	
	template< typename T, typename ObjectFactoryClass >
	bool operator!=( ptr_vector< T, ObjectFactoryClass > &lhs, owned_ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return !( lhs == rhs );
	}
	
	template< typename T, typename ObjectFactoryClass >
	bool operator<( ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator<( owned_ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator<( ptr_vector< T, ObjectFactoryClass > &lhs, owned_ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator<=( ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::less_equal< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator<=( owned_ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::less_equal< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator<=( ptr_vector< T, ObjectFactoryClass > &lhs, owned_ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::less_equal< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator>( ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::greater< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator>( owned_ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::greater< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator>( ptr_vector< T, ObjectFactoryClass > &lhs, owned_ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::greater< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator>=( ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::greater_equal< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator>=( owned_ptr_vector< T, ObjectFactoryClass > &lhs, ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::greater_equal< T >() );
	}

	template< typename T, typename ObjectFactoryClass >
	bool operator>=( ptr_vector< T, ObjectFactoryClass > &lhs, owned_ptr_vector< T, ObjectFactoryClass > &rhs )
	{
		return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::greater_equal< T >() );
	}

	template< typename T > class DefaultArrayAlloc;

	template< typename T, typename ArrayAllocClass = DefaultArrayAlloc< T > > class owned_array_ptr_vector;

	///	Stores a collection of pointers to arrays, but does not manage those arrays' lifetimes.
	/**	The array_ptr_vector class behaves like the STL std::vector class, but stores
		pointers to arrays, instead of storing objects by value.  array_ptr_vector does
		<em>not</em> take responsibility for the proper deallocation of the arrays
		that its pointers reference; it is intended as a way of moving a collection
		of pointers that are owned elsewhere.  The owned_array_ptr_vector is designed to
		own a collection of arrays and manage their proper deallocation.

		The array_ptr_vector interface is nearly identical to that of the STL std::vector.
		The documentation below only describes in detail those features which differ
		from the STL std::vector API.  Please see an STL reference for more information
		about the details of the interface.

		Unlike the owned_array_ptr_vector, the array_ptr_vector is freely copyable.

		The array_ptr_vector is designed to be usable in conjunction with instances of the 
		owned_array_ptr_vector class. In order to facilitate interoperability, it is necessary 
		for the ptr_vector to take the type parameter ArrayAllocClass.  In most cases 
		this should be left to the default, but in cases where the array_ptr_vector must interact 
		with an	owned_array_ptr_vector that has a nonstandard ArrayAllocClass, the array_ptr_vector
		must be declared with an ArrayAllocClass that matches the ArrayAllocClass
		of the owned_array_ptr_vector.  However, the ArrayAllocClass is not otherwise used
		by the array_ptr_vector, and has no effect on its operation.

		\param	T	The type of the array elements of the arrays to which the stored pointers point.
		\param	ArrayAllocClass	When interacting with owned_array_ptr_vector objects, should
									be set to match the ArrayAllocClass of the 
									owned_array_ptr_vector.
	*/
	template< typename T, typename ArrayAllocClass = DefaultArrayAlloc< T > >
	class array_ptr_vector
	{
		friend class owned_array_ptr_vector< T, ArrayAllocClass >;

		public:
			///	STL-compatible random access iterator.
			class iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_array_ptr_vector< T, ArrayAllocClass >;
				friend class array_ptr_vector< T, ArrayAllocClass >;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					iterator( typename std::vector< T * >::iterator iter )
					{
						this->iter = iter;
					}

					iterator( const iterator &iter )
					{
						this->iter = iter.iter;
					}

					~iterator( void ) {  }

					iterator &operator=( const iterator &src )
					{
						this->iter = src.iter;
						return *this;
					}

					bool operator==( const iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const iterator &a )
					{
						return this->iter != a.iter;
					}

					T *operator*( void )
					{
						return *( this->iter );
					}

					iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					iterator operator++( int )		//postfix ++
					{
						iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					iterator operator--( int )		//postfix --
					{
						iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					iterator operator+( size_type n )
					{
						iterator tmp( *this );
						tmp += n;
						return tmp;
					}

					iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					iterator operator-( size_type n )
					{
						iterator tmp( *this );
						tmp -= n;
						return tmp;
					}

					difference_type operator-( const iterator &a )
					{
						return this->iter - a.iter;
					}

					T *operator[]( size_type n )
					{
						return this->iter[ n ];
					}

					bool operator<( const iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					iterator( void );

					typename std::vector< T * >::iterator iter;
			};

			///	STL-compatible random access iterator for const data.
			class const_iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_array_ptr_vector< T, ArrayAllocClass >;
				friend class array_ptr_vector< T, ArrayAllocClass >;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					const_iterator( typename std::vector< T * >::const_iterator citer )
					{
						this->iter = citer;
					}

					const_iterator( const const_iterator &citer )
					{
						this->iter = citer.iter;
					}

					const_iterator( const iterator &iter )
					{
						this->iter = iter.iter;
					}

					~const_iterator( void ) {  }

					const_iterator &operator=( const const_iterator &src )
					{
						this->iter = src.iter;
					}

					bool operator==( const const_iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const const_iterator &a )
					{
						return this->iter != a.iter;
					}

					const T *operator*( void )
					{
						return *( this->iter );
					}

					const_iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					const_iterator operator++( int )	//postfix ++
					{
						iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					const_iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					const_iterator operator--( int )	//postfix --
					{
						iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					const_iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					const_iterator operator+( size_type n )
					{
						const_iterator tmp( *this );
						tmp += n;
						return tmp;
					}

					const_iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					const_iterator operator-( size_type n )
					{
						const_iterator tmp( *this );
						tmp -= n;
						return tmp;
					}

					difference_type operator-( const const_iterator &a )
					{
						return this->iter - a.iter;
					}

					const T *operator[]( size_type n )
					{
						return this->iter[ n ];
					}

					bool operator<( const const_iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const const_iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const const_iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const const_iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					const_iterator( void );

					typename std::vector< T * >::const_iterator iter;
			};

			typedef T* reference;
			typedef const T* const_reference;
			typedef T** pointer;
			typedef const T** const_pointer;
			typedef size_t size_type;
			typedef ptrdiff_t difference_type;
			typedef T* value_type;
			typedef std::reverse_iterator< iterator > reverse_iterator;
			typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

			///	Constructs an empty array_ptr_vector.
			array_ptr_vector( void ) : v() {  }

			///	Constructs an array_ptr_vector as a copy of another array_ptr_vector.
			/**	After construction, the new collection is a copy of the original:  It contains
				the same pointers referencing the same arrays as the original.  However, the
				two are not permanently synchronized, and their contents may freely diverge.
			*/
			array_ptr_vector( const array_ptr_vector< T, ArrayAllocClass > &x ) : v( x.v ) {  }

			///	Constructs an array_ptr_vector referencing the arrays in an owned_array_ptr_vector.
			/**	After construction, the new collection is a copy of the original:  It contains
				the same pointers referencing the same arrays as the original.  However, the
				two are not permanently synchronized, and their contents may freely diverge.
			*/
			array_ptr_vector( const owned_array_ptr_vector< T, ArrayAllocClass > &x ) : v( x.v ) {  }

			///	Constructs an array_ptr_vector referencing a subset of the arrays in another array_ptr_vector.
			array_ptr_vector( const_iterator first, const_iterator last )
			{
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			///	Constructs an array_ptr_vector referencing a subset of the arrays in an owned_array_ptr_vector.
			array_ptr_vector( typename owned_array_ptr_vector< T, ArrayAllocClass >::const_iterator first, 
				typename owned_array_ptr_vector< T, ArrayAllocClass >::const_iterator last )
			{
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			~array_ptr_vector( void ) {  }

			///	Copies the stored pointers from another array_ptr_vector.
			/**	After construction, the new collection is a copy of the original:  It contains
				the same pointers referencing the same arrays as the original.  However, the
				two are not permanently synchronized, and their contents may freely diverge.
			*/
			array_ptr_vector< T, ArrayAllocClass > &operator=( const array_ptr_vector< T, ArrayAllocClass > &x )
			{
				v = x.v;
				return *this;
			}

			///	Copies the stored pointers from an owned_array_ptr_vector.
			/**	After construction, the new collection is a copy of the original:  It contains
				the same pointers referencing the same arrays as the original.  However, the
				two are not permanently synchronized, and their contents may freely diverge.
			*/
			array_ptr_vector< T, ArrayAllocClass > &operator=( const owned_array_ptr_vector< T, ArrayAllocClass > &x )
			{
				v = x.v;
				return *this;
			}

			///	Clears the container, and then copies a subset of the stored pointers from another array_ptr_vector.
			void assign( const_iterator first, const_iterator last )
			{
				v.clear();
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			///	Clears the container, and then copies a subset of the stored pointers from an owned_array_ptr_vector.
			void assign( typename owned_array_ptr_vector< T, ArrayAllocClass >::const_iterator first, 
				typename owned_array_ptr_vector< T, ArrayAllocClass >::const_iterator last )
			{
				v.clear();
				while( first != last )
				{
					v.push_back( first.get_ptr() );
					first++;
				}
			}

			///	Instructs the container to allocate enough memory for at least a certain number of stored pointers.
			void reserve( size_type n )
			{
				v.reserve( n );
			}

			///	Exchanges the contents of two array_ptr_vector instances.
			void swap( array_ptr_vector< T, ArrayAllocClass > &x )
			{
				v.swap( x.v );
			}

			///	Returns an iterator referencing the first element in the collection.
			iterator begin( void )
			{
				return iterator( v.begin() );
			}

			///	Returns a const_iterator referencing the first element in the collection.
			const_iterator begin( void ) const
			{
				return const_iterator( v.begin() );
			}

			///	Returns an iterator referencing the end of the collection.
			iterator end( void )
			{
				return iterator( v.end() );
			}

			///	Returns a const_iterator referencing the end of the collection.
			const_iterator end( void ) const
			{
				return const_iterator( v.end() );
			}

			///	Returns a reverse_iterator referencing the first element in the collection for reverse order traversal.
			reverse_iterator rbegin( void )
			{
				return reverse_iterator( v.rbegin() );
			}

			///	Returns a const_reverse_iterator referencing the first element in the collection for reverse order traversal.
			const_reverse_iterator rbegin( void ) const
			{
				return const_reverse_iterator( v.rbegin() );
			}

			///	Returns a reverse_iterator referencing the end of the collection for reverse order traversal.
			reverse_iterator rend( void )
			{
				return reverse_iterator( v.rend() );
			}

			///	Returns a const_reverse_iterator referencing the end of the collection for reverse order traversal.
			const_reverse_iterator rend( void ) const
			{
				return const_reverse_iterator( v.rend() );
			}

			///	Returns the number of stored pointers in the collection.
			size_type size( void ) const
			{
				return v.size();
			}

			///	Returns the maximum number of pointers that can be stored in the collection.
			size_type max_size( void ) const
			{
				return v.max_size();
			}

			///	Returns the number of pointers that can be stored without allocating new memory.
			size_type capacity( void ) const
			{
				return v.capacity();
			}

			///	Tests whether or not the container is empty.
			bool empty() const
			{
				return v.empty();
			}

			///	Returns a reference to a specific array in the collection.
			reference operator[]( size_type n )
			{
				return v[ n ];
			}

			///	Returns a const_reference to a specific array in the collection.
			const_reference operator[]( size_type n ) const
			{
				return v[ n ];
			}

			///	Returns a reference to a specific array in the collection, after checking the validity of the index.
			reference at( size_type n )
			{
				return v.at( n );
			}

			///	Returns a const_reference to a specific array in the collection, after checking the validity of the index.
			const_reference at( size_type n ) const
			{
				return v.at( n );
			}

			///	Returns a reference to the first array in the collection.
			reference front()
			{
				return v.front();
			}

			///	Returns a const_reference to the first array in the collection.
			const_reference front() const
			{
				return v.front();
			}

			///	Returns a reference to the last array in the collection.
			reference back()
			{
				return v.back();
			}

			///	Returns a reference to the last array in the collection.
			const_reference back() const
			{
				return v.back();
			}

			///	Adds an array to the collection.
			void push_back( T *x )
			{
				v.push_back( x );
			}

			///	Inserts an array into the collection at a specific position.
			iterator insert( iterator position, T *x )
			{
				return iterator( v.insert( position.iter, x ) );
			}

			///	Copies a group of stored pointers from another array_ptr_vector, and inserts them into the collection at a specific position.
			void insert( iterator position, const_iterator first, const_iterator last )
			{
				while( first != last )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Copies a group of stored pointers from an owned_array_ptr_vector, and inserts them into the collection at a specific position.
			void insert( iterator position, typename owned_array_ptr_vector< T, ArrayAllocClass >::const_iterator first, 
				typename owned_array_ptr_vector< T, ArrayAllocClass >::const_iterator last )
			{
				while( first != last )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Removes the last element from the collection.
			void pop_back( void )
			{
				v.pop_back();
			}

			///	Removes a specified element from the collection.
			iterator erase( iterator position )
			{
				return iterator( v.erase( position.iter ) );
			}

			///	Removes a specified group of elements from the collection.
			iterator erase( iterator first, iterator last )
			{
				return iterator( v.erase( first.iter, last.iter ) );
			}

			///	Removes all elements from the collection.
			void clear( void )
			{
				v.clear();
			}
			
		private:
			std::vector< T * > v;

	};

	///	Stores a collection of pointers to arrays, and manages those arrays' lifetimes.
	/**	The owned_array_ptr_vector class behaves like the STL std::vector class, but stores
		pointers to arrays instead of the objects themselves.  owned_array_ptr_vector assumes
		ownership of the arrays referenced by its pointers, meaning that it takes
		responsibility for their proper deallocation.  The array_ptr_vector is designed to
		have a similar interface without owning its arrays.

		Unlike the array_ptr_vector, the owned_array_ptr_vector is neither copyable nor assignable.
		This requirement is imposed due to the unique considerations of array ownership.
		Arrays can be transfered between owned_array_ptr_vector instances, as described
		below, but such an action must be undertaken explicitly by the user.

		Storing arrays by reference requires slightly different semantics for adding
		and removing elements than those of the std::vector, which stores objects by value.
		In particular, arrays are not copyable, and	in general copy semantics would be 
		inappropriate for manipulating them.  Yet at the same time, the references cannot 
		freely be copied to other owned_array_ptr_vectors, since a heap-allocated array 
		can only be owned by one object.  

		As a consequence, the part of the std::vector interface that normally creates or
		copies objects has been modified to manage provide member functions with
		appropriate semantics.  The assign member function which would normally copy
		the elements from one vector to another has been replaced by one that takes 
		ownership of unowned objects.  The form of assign that would read an input 
		iterator to	create vector elements has been removed.  A variant of assign has
		been implemented that populates the collection with newly allocated arrays of a
		given size, initialized to specified value.  Similar adjustments have been made 
		for the insert, replace, resize and push_back member functions.  
		
		Additionally, a transfer() member function has been added that moves arrays
		between between owned_array_ptr_vector instances.  Thus a distinction is made between
		the "standard" interface members assign, insert, replace and push_back, which
		have versions that can take ownership of unowned arrays, and the transfer()
		members, which moves ownership between two owned_array_ptr_vector instances.

		The member functions that delete elements from vectors here have the added
		function of deallocating the arrays referenced.  A release() member has also
		been added that allows one to obtain ownership of one or more objects, removing
		them from the owned_array_ptr_vector without destroying them.

		The second template parameter of this class is the ArrayAllocClass, which
		allows a user to specify an object class which manages the allocation and
		deallocation of any arrays created by the owned_array_ptr_vector.  The
		ArrayAllocClass must implement two member functions, alloc() which obtains
		a new array of a given size, and dealloc() which deallocates arrays.  The
		former must accept a size_t with the number of elements that the array should
		contain and return a T*; the latter must accept a T* and deallocate that
		specific array.

		A default ArrayAllocClass is provided, and will be used if an alternative
		template parameter value is not given.  This default class, DefaultArrayAlloc,
		should be sufficient for almost all cases.  It allocates arrays by calling
		the new [] operator, and deallocates them by calling the delete [] operator.
		The ArrayAllocClass does not initialize the elements of the newly-allocated
		arrays.  If the array needs to be initialized automatically, a custom ArrayAllocClass
		should be supplied to carry out the initialization.
		
		The documentation below only describes in detail those features which differ
		from the STL std::vector API.  Please see an STL reference for more information
		about the details of the std::vector interface.

		This class is designed for use in conjunction with the array_ptr_vector class, 
		which behaves similarly, but does not own the elements referenced by its pointers.
		In particular, array_ptr_vector instances are used by this class whenever references 
		to multiple arrays are involved.  The only special consideration when working with
		array_ptr_vectors and owned_array_ptr_vectors is that the array_ptr_vectors must be 
		declared to	have the same ArrayAllocClass type as the owned_array_ptr_vector.  This is
		required despite the fact that the array_ptr_vector does not actually use the allocator
		object and does not hold an instance of it (the requirement arises from the
		C++ type system).

		\param	T	The type of the array elements in the arrays to which the stored pointers point.
		\param	ArrayAllocClass	(optional) The array allocator/deallocator type to be used.
	*/
	template< typename T, typename ArrayAllocClass >
	class owned_array_ptr_vector
	{
		friend class array_ptr_vector< T, ArrayAllocClass >;

		public:
			///	STL-compatible random access iterator.
			class iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_array_ptr_vector< T, ArrayAllocClass >;
				friend class array_ptr_vector< T, ArrayAllocClass >;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					iterator( typename std::vector< T * >::iterator iter )
					{
						this->iter = iter;
					}

					iterator( const iterator &iter )
					{
						this->iter = iter.iter;
					}

					~iterator( void ) {  }

					iterator &operator=( const iterator &src )
					{
						this->iter = src.iter;
						return *this;
					}

					bool operator==( const iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const iterator &a )
					{
						return this->iter != a.iter;
					}

					T *operator*( void )
					{
						return *( this->iter );
					}

					iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					iterator operator++( int )		//postfix ++
					{
						iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					iterator operator--( int )		//postfix --
					{
						iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					iterator operator+( size_type n )
					{
						iterator tmp( *this );
						tmp += n;
						return tmp;
					}

					iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					iterator operator-( size_type n )
					{
						iterator tmp( *this );
						tmp -= n;
						return tmp;
					}

					difference_type operator-( const iterator &a )
					{
						return this->iter - a.iter;
					}

					T *operator[]( size_type n )
					{
						return this->iter[ n ];
					}

					bool operator<( const iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					iterator( void );

					typename std::vector< T * >::iterator iter;
			};

			///	STL-compatible random access iterator for const data.
			class const_iterator : public std::iterator< std::random_access_iterator_tag, T >
			{
				friend class owned_array_ptr_vector< T, ArrayAllocClass >;
				friend class array_ptr_vector< T, ArrayAllocClass >;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					const_iterator( typename std::vector< T * >::const_iterator citer )
					{
						this->iter = citer;
					}

					const_iterator( const const_iterator &citer )
					{
						this->iter = citer.iter;
					}

					const_iterator( const iterator &iter )
					{
						this->iter = iter.iter;
					}

					~const_iterator( void ) {  }

					const_iterator &operator=( const const_iterator &src )
					{
						this->iter = src.iter;
					}

					bool operator==( const const_iterator &a )
					{
						return this->iter == a.iter;
					}

					bool operator!=( const const_iterator &a )
					{
						return this->iter != a.iter;
					}

					const T *operator*( void )
					{
						return *( this->iter );
					}

					const_iterator &operator++( void )	//prefix ++
					{
						this->iter++;
						return *this;
					}

					const_iterator operator++( int )	//postfix ++
					{
						const_iterator tmp( *this );
						this->iter++;
						return tmp;
					}

					const_iterator &operator--( void )	//prefix --
					{
						this->iter--;
						return *this;
					}

					const_iterator operator--( int )	//postfix --
					{
						const_iterator tmp( *this );
						this->iter--;
						return tmp;
					}

					const_iterator &operator+=( size_type n )
					{
						this->iter += n;
						return *this;
					}

					const_iterator operator+( size_type n )
					{
						const_iterator tmp( *this );
						tmp += n;
						return *tmp;
					}

					const_iterator &operator-=( size_type n )
					{
						this->iter -= n;
						return *this;
					}

					const_iterator operator-( size_type n )
					{
						const_iterator tmp( *this );
						tmp -= n;
						return tmp;
					}

					difference_type operator-( const const_iterator &a )
					{
						return this->iter - a.iter;
					}

					const T *operator[]( size_type n )
					{
						return this->iter[ n ];
					}

					bool operator<( const const_iterator &a )
					{
						return this->iter < a.iter;
					}

					bool operator<=( const const_iterator &a )
					{
						return this->iter <= a.iter;
					}

					bool operator>( const const_iterator &a )
					{
						return this->iter > a.iter;
					}

					bool operator>=( const const_iterator &a )
					{
						return this->iter >= a.iter;
					}

					T *get_ptr( void )
					{
						return *( this->iter );
					}

				private:
					const_iterator( void );

					typename std::vector< T * >::const_iterator iter;
			};


			typedef T& reference ;
			typedef const T& const_reference;
			typedef T* pointer;
			typedef const T* const_pointer;
			typedef size_t size_type;
			typedef ptrdiff_t difference_type;
			typedef T value_type;
			typedef std::reverse_iterator< iterator > reverse_iterator;
			typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

			///	Constructs an empty owned_array_ptr_vector.
			/**	\param	inalloc	(optional) An array allocator instance from which to construct the resident array allocator.
			*/
			owned_array_ptr_vector( ArrayAllocClass inalloc = ArrayAllocClass() ) : v(), alloc( inalloc ) {  }

			///	Constructs an owned_array_ptr_vector and fills with a given number of new arrays.
			/**	The owned_array_ptr_vector is filled with n heap-allocated arrays,
				each of size arraysize.  The resident array allocator is used to
				construct, and if applicable initialize, the arrays.

				\param	n	The number of arrays to create
				\param	arraysize The number of elements to include in each array.
				\param	inalloc	(optional) An array allocator instance from which to construct the resident array allocator.
			*/
			explicit owned_array_ptr_vector( size_type n, size_type arraysize, ArrayAllocClass &inalloc = ArrayAllocClass() ) 
				: v( n ), alloc( inalloc )
			{
				for( size_type i = 0; i < n; i++ )
					v[ n ] = alloc.alloc( arraysize );
			}

			///	Constructs an owned_array_ptr_vector by taking ownership of arrays referenced in an array_ptr_vector.
			/**	\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the arrays before adding.  Do NOT add
							stack-allocated arrays to the collection.

				\param	first	Iterator pointing to the first array to be owned.
				\param	last	Iterator pointing one unit past the last array to be owned.
				\param	inalloc	(optional) An array allocator instance from which to construct the resident array allocator.
			*/
			owned_array_ptr_vector( typename array_ptr_vector< T, ArrayAllocClass >::const_iterator first, 
				typename array_ptr_vector< T, ArrayAllocClass >::const_iterator last, ArrayAllocClass inalloc = ArrayAllocClass() )
				: alloc( inalloc )
			{
				while( first != last )
					v.push_back( &*first++ );
			}

			///	Constructs an owned_array_ptr_vector by transferring ownership from another owned_array_ptr_vector.
			/**	This member function is intended to support the safe transfer of a collection of
				arrays between two owned_array_ptr_vector instances, where the transfer() member
				function is not feasible.  Although this interface is somewhat cumbersome, it
				ensures exception safety under all circumstances.

				The design of this mechanism was intended to support circumstances where multiple 
				arrays need to pass between an owned_array_ptr_vector in one scope and a newly
				created owned_array_ptr_vector in another scope, without the two having direct
				contact.  This could occur when a function A needs to pass a collection of
				arrays to a function B that it calls, without passing along direct access
				to A's owned_array_ptr_vector (e.g. if the starting owned_array_ptr_vector is a private
				member of a data structure that B is restricted from accessing).  The
				release_safe() member function is used to obtain the exception-safe
				collection from the origin container, which is then passed along to the
				destination.

				The mechanism of exchange is a temporary heap-allocated owned_array_ptr_vector held
				in a std::auto_ptr.  The std::auto_ptr wrapper ensures that the heap-allocated
				owned_array_ptr_vector is automatically deleted after the exchange, while the use of
				an owned_array_ptr_vector as the medium for exchange ensures that the objects
				being transfered are not leaked if the exchange falls through.

				If exception safety is not important, the objects can be transferred via
				stack-allocated array_ptr_vector instances instead.  The first is obtained via the
				release() member function, which returns an array_ptr_vector.  This can be copied as
				needed to complete the exchange.  This method is not generally recommended,
				however.

				\param	invector	Container of arrays to add.
				\param	inalloc	(optional) An array allocator instance from which to construct the resident array allocator.
			*/
			owned_array_ptr_vector( std::auto_ptr< owned_array_ptr_vector< T, ArrayAllocClass > > &invector, 
				ArrayAllocClass inalloc = ArrayAllocClass() ) : alloc( inalloc )
			{
				this->swap( *invector );
			}

			~owned_array_ptr_vector( void )
			{
				for( typename std::vector< T * >::iterator i = v.begin(); i != v.end(); i++ )
					alloc.dealloc( *i );
			}

			///	Clears the container, and creates a certain number of new arrays.
			/**	The owned_array_ptr_vector is filled with n heap-allocated arrays,
				each of size arraysize.  The resident array allocator is used to
				construct, and if applicable initialize, the arrays.

				Any arrays referenced previously by the owned_array_ptr_vector are deallocated.

				If a pointer to an array allocator instance is supplied, that particular
				instance will be used for creating new arrays.  The resident array allocator
				would not be affected.

				\param	n	The number of arrays to create
				\param	arraysize The number of elements to include in each array.
				\param	builder	(optional) An array allocator instance to use for the duration of this call.
			*/
			void assign_create( size_type n, size_type arraysize, ArrayAllocClass *builder = 0 )
			{
				if( !builder ) builder = &alloc;

				this->clear();
				for( size_type i = 0; i < n; i++ )
					v.push_back( builder->alloc( arraysize ) );
			}

			///	Clears the container, and takes ownership of a subset of arrays in an array_ptr_vector.
			/**	Any arrays referenced previously by the owned_array_ptr_vector are deallocated.

				\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the arrays before adding.  Do NOT add
							stack-allocated arrays to the collection.

				\param	first	Iterator pointing to the first array to be owned.
				\param	last	Iterator pointing one unit past the last array to be owned.
			*/
			void assign_takeownership( typename array_ptr_vector< T, ArrayAllocClass >::const_iterator first, 
				typename array_ptr_vector< T, ArrayAllocClass >::const_iterator last )
			{
				this->clear();
				while( first != last )
					v.push_back( &*first++ );
			}

			///	Clears the container, and takes ownership of all the arrays in an array_ptr_vector.
			/**	Any arrays referenced previously by the owned_array_ptr_vector are deallocated.

				\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the arrays before adding.  Do NOT add
							stack-allocated arrays to the collection.

				\param	container	Container of arrays to add.
			*/
			void assign_takeownership( array_ptr_vector< T, ArrayAllocClass > &container )
			{
				this->clear();
				v.swap( container.v );
			}

			///	Clears the contents of the container, and takes ownership of the arrays in another owned_array_ptr_vector.
			/**	This member function is intended to support the safe transfer of a collection of
				arrays between two owned_array_ptr_vector instances, where the transfer() member
				function is not feasible.  Although this interface is somewhat cumbersome, it
				ensures exception safety under all circumstances.

				The design of this mechanism was intended to support circumstances where multiple 
				arrays need to pass between an owned_array_ptr_vector in one scope and a newly
				created owned_array_ptr_vector in another scope, without the two having direct
				contact.  This could occur when a function A needs to return a collection of
				arrays to a function B that called it, without having direct access
				to B's owned_array_ptr_vector (e.g. if the starting owned_array_ptr_vector is a private
				member of a data structure that A is restricted from accessing).  The
				release_safe() member function is used to obtain the exception-safe
				collection from the origin container, which is then passed along to the
				destination.

				The mechanism of exchange is a temporary heap-allocated owned_array_ptr_vector held
				in a std::auto_ptr.  The std::auto_ptr wrapper ensures that the heap-allocated
				owned_array_ptr_vector is automatically deleted after the exchange, while the use of
				an owned_array_ptr_vector as the medium for exchange ensures that the arrays
				being transfered are not leaked if the exchange falls through.

				If exception safety is not important, the arrays can be transferred via
				stack-allocated array_ptr_vector instances instead.  The first is obtained via the
				release() member function, which returns an array_ptr_vector.  This can be copied as
				needed to complete the exchange.  This method is not generally recommended,
				however.

				Any objects referenced previously by the owned_array_ptr_vector are deallocated.

				\param	invector	The collection of objects to take ownership of.
			*/
			void assign_takeownership( std::auto_ptr< owned_array_ptr_vector< T, ArrayAllocClass > > &invector )
			{
				this->clear();
				v.swap( invector->v );
			}

			///	Returns the number of elements stored in the collection.
			size_type size( void ) const
			{
				return v.size();
			}

			///	Returns the maximum number of pointers that can be stored.
			size_type max_size( void ) const
			{
				return v.max_size();
			}

			///	Resizes the collection, allocating or deallocating new arrays as needed.
			/**	Resizes the container, deallocating arrays or creating new arrays as needed.

				If the new size is less than the current size, arrays at the end of the 
				vector are deallocated until the desired size is reached.
			
				If the new size is greater than the current size, new heap-allocated arrays 
				are created and appended to the collection, each of size arraysize.  The 
				resident array allocator is used to construct, and if applicable initialize, 
				the arrays.

				If a pointer to an array allocator instance is supplied, that particular
				instance will be used for creating new arrays.  The resident array allocator
				would not be affected.

				\param	sz	The new size for the collection.
				\param	arraysize	If new arrays must be created, the size of each array.
				\param	builder	(optional) An array allocator instance to use for the duration of this call.
			*/
			void resize( size_type sz, size_type arraysize, ArrayAllocClass *builder = 0 )
			{
				if( !builder ) builder = &alloc;

				if( sz < v.size() )
				{
					while( v.size() > sz )
					{
						alloc.dealloc( v.back() );
						v.pop_back();
					}
				}
				else if( sz > v.size() )
				{
					while( v.size() < sz )
						v.push_back( builder->alloc( arraysize ) );
				}
			}

			///	Returns the number of arrays that can be stored without allocating new memory.
			size_type capacity( void ) const
			{
				return v.capacity();
			}

			///	Tests whether or not the container is empty.
			bool empty() const
			{
				return v.empty();
			}

			///	Instructs the container to allocate enough space to store at least a certain number of pointers.
			void reserve( size_type n )
			{
				v.reserve( n );
			}

			///	Exchanges the contents of two owned_array_ptr_vector instances.
			void swap( owned_array_ptr_vector< T, ArrayAllocClass > &x )
			{
				v.swap( x.v );
			}

			///	Returns an iterator referencing the first element in the collection.
			iterator begin( void )
			{
				return iterator( v.begin() );
			}

			///	Returns a const_iterator referencing the first element in the collection.
			const_iterator begin( void ) const
			{
				return const_iterator( v.begin() );
			}

			///	Returns an iterator referencing the end of the collection.
			iterator end( void )
			{
				return iterator( v.end() );
			}

			///	Returns a const_iterator referencing the end of the collection.
			const_iterator end( void ) const
			{
				return const_iterator( v.end() );
			}

			///	Returns an reverse_iterator referencing the first element in the collection for reverse traversal.
			reverse_iterator rbegin( void )
			{
				return reverse_iterator( v.rbegin() );
			}

			///	Returns a const_reverse_iterator referencing the first element in the collection for reverse traversal.
			const_reverse_iterator rbegin( void ) const
			{
				return const_reverse_iterator( v.rbegin() );
			}

			///	Returns a reverse_iterator referencing the end of the collection for reverse traversal.
			reverse_iterator rend( void )
			{
				return reverse_iterator( v.rend() );
			}

			///	Returns a const_reverse_iterator referencing the end of the collection for reverse traversal.
			const_reverse_iterator rend( void ) const
			{
				return const_reverse_iterator( v.rend() );
			}

			///	Returns a reference to the array at a specified position.
			T *operator[]( size_type n )
			{
				return v[ n ];
			}

			///	Returns a const_reference to the array at a specified position.
			const T *operator[]( size_type n ) const
			{
				return v[ n ];
			}

			///	Returns a reference to the array at a specified position, after checking the validity of the index.
			T *at( size_type n )
			{
				return v.at( n );
			}

			///	Returns a const_reference to the array at a specified position, after checking the validity of the index.
			const T *at( size_type n ) const
			{
				return v.at( n );
			}

			///	Returns a reference to the first element in the collection.
			T *front()
			{
				return v.front();
			}

			///	Returns a const_reference to the first element in the collection.
			const T *front() const
			{
				return v.front();
			}

			///	Returns a reference to the last element in the collection.
			T *back()
			{
				return v.back();
			}

			///	Returns a const_reference to the last element in the collection.
			const T *back() const
			{
				return v.back();
			}

			///	Adds a new array to the end of the collection.
			/**	\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the array before adding.  Do NOT add
							stack-allocated arrays to the collection.
			*/
			void push_back( T *x )
			{
				v.push_back( x );
			}

			///	Inserts an array into the collection at a specified position.
			/**	\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the array before adding.  Do NOT add
							stack-allocated arrays to the collection.
			*/
			iterator insert( iterator position, T *x )
			{
				return iterator( v.insert( position.iter, x ) );
			}

			///	Creates a specified number of new arrays, and inserts them into the collection at the specified position.
			/**	The specified number of heap-allocated arrays, each of size arraysize,
				are created and inserted into the container.  The resident array allocator 
				is used to construct, and if applicable initialize, the arrays.

				If a pointer to an array allocator instance is supplied, that particular
				instance will be used for creating new arrays.  The resident array allocator
				would not be affected.

				\param	position	The position at which to insert the arrays.
				\param	n	The number of arrays to create
				\param	arraysize The number of elements to include in each array.
				\param	builder	(optional) An array allocator instance to use for the duration of this call.
			*/
			void insert_create( iterator position, size_type n, size_type arraysize, ArrayAllocClass *builder = 0 )
			{
				if( !builder ) builder = &alloc;

				for( size_type i = 0; i < n; i++ )
				{
					position = iterator( v.insert( position.iter, builder->alloc( arraysize ) ) );
					position++;
				}
			}

			///	Assumes ownership of a subset of arrays from an array_ptr_vector, and inserts them into the collection at the specified position.
			/**	\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the arrays before adding.  Do NOT add
							stack-allocated arrays to the collection.
			*/
			void insert_takeownership( iterator position, typename array_ptr_vector< T, ArrayAllocClass >::const_iterator first, 
				typename array_ptr_vector< T, ArrayAllocClass >::const_iterator last )
			{
				while( first != last )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Assumes ownership of all the arrays from an array_ptr_vector, and inserts them into the collection at the specified position.
			/**	\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the arrays before adding.  Do NOT add
							stack-allocated arrays to the collection.
			*/
			void insert_takeownership( iterator position, array_ptr_vector< T, ArrayAllocClass > &container )
			{
				typename array_ptr_vector< T >::iterator first = container.begin();
				while( first != container.end() )
				{
					position = iterator( v.insert( position.iter, first.get_ptr() ) );
					position++;
					first++;
				}
			}

			///	Takes ownership of the arrays in another owned_array_ptr_vector, and inserts them in the collection at a specified position.
			/**	This member function is intended to support the safe transfer of a collection of
				arrays between two owned_array_ptr_vector instances, where the transfer() member
				function is not feasible.  Although this interface is somewhat cumbersome, it
				ensures exception safety under all circumstances.

				The design of this mechanism was intended to support circumstances where multiple 
				arrays need to pass between an owned_array_ptr_vector in one scope and a newly
				created owned_array_ptr_vector in another scope, without the two having direct
				contact.  This could occur when a function A needs to return a collection of
				arrays to a function B that called it, without having direct access
				to B's owned_array_ptr_vector (e.g. if the starting owned_array_ptr_vector is a private
				member of a data structure that A is restricted from accessing).  The
				release_safe() member function is used to obtain the exception-safe
				collection from the origin container, which is then passed along to the
				destination.

				The mechanism of exchange is a temporary heap-allocated owned_array_ptr_vector held
				in a std::auto_ptr.  The std::auto_ptr wrapper ensures that the heap-allocated
				owned_array_ptr_vector is automatically deleted after the exchange, while the use of
				an owned_array_ptr_vector as the medium for exchange ensures that the arrays
				being transfered are not leaked if the exchange falls through.

				If exception safety is not important, the arrays can be transferred via
				stack-allocated array_ptr_vector instances instead.  The first is obtained via the
				release() member function, which returns an array_ptr_vector.  This can be copied as
				needed to complete the exchange.  This method is not generally recommended,
				however.

				\param	position	The position where the arrays are to be inserted.
				\param	invector	The collection of arrays to take ownership of.
			*/
			void insert_takeownership( iterator position, std::auto_ptr< owned_ptr_vector< T, ArrayAllocClass > > &invector )
			{
				this->transfer( position, *invector );
			}

			///	Transfers ownership of all the arrays in another owned_array_ptr_vector, and inserts them into the collection at the specified position.
			void transfer( iterator position, owned_array_ptr_vector< T, ArrayAllocClass > &prev_owner )
			{
				array_ptr_vector< T > newvector = prev_owner.release();
				this->insert( position, newvector.begin(), newvector.end() );
			}

			///	Transfers ownership of one specific array from another owned_array_ptr_vector, and inserts it into the collection at the specified position.
			void transfer( iterator position, iterator obj, owned_array_ptr_vector< T, ArrayAllocClass > &prev_owner )
			{
				v.insert( position.iter, obj.get_ptr() );
				prev_owner.release( obj );
			}

			///	Transfers ownership of a subset of arrays from another owned_array_ptr_vector, and inserts them into the collection at the specified position.
			void transfer( iterator position, iterator first, iterator last, owned_array_ptr_vector< T, ArrayAllocClass > &prev_owner )
			{
				iterator first2( first );
				while( first2 != last )
				{
					position = iterator( v.insert( position.iter, first2.get_ptr() ) );
					position++;
					first2++;
				}
				prev_owner.release( first, last );
			}

			///	Replaces the array at a specified position in the collection.
			/**	The array previously at the specified position is deallocated.
				
				\warning	Be certain that it is safe for the owned_array_ptr_vector to
							assume ownership of the arrays before adding.  Do NOT add
							stack-allocated arrays to the collection.
			*/
			void replace( iterator position, T *x )
			{
				alloc.dealloc( *( position.iter ) );
				*( position.iter ) = x;
			}

			///	Removes and deallocates the last array in the collection.
			void pop_back( void )
			{
				alloc.dealloc( v.back() );
				v.pop_back();
			}

			///	Removes and deallocates the array at a specified position in the collection.
			iterator erase( iterator position )
			{
				alloc.dealloc( position.get_ptr() );
				return iterator( v.erase( position.iter ) );
			}

			///	Removes and deallocates a subset of the arrays in the collection.
			iterator erase( iterator first, iterator last )
			{
				iterator first2 = first;
				while( first2 != last )
				{
					alloc.dealloc( first2.get_ptr() );
					first2++;
				}
				return iterator( v.erase( first.iter, last.iter ) );
			}

			///	Removes and releases ownership of a specified array in the collection.
			/**	The array is removed from the collection, and the pointer to the array
				is returned.
				
				\warning	Using this member function to transfer ownership may not be
							exception-safe.  The release_safe() function is preferred.
			*/
			T *release( iterator position )
			{
				T *tmp = position.get_ptr();
				v.erase( position.iter );
				return tmp;
			}

			///	Removes and releases ownership of a subset of arrays in the collection.
			/**	The arrays are removed from the collection, and an array_ptr_vector
				containing pointers to them is returned.
				
				\warning	Using this member function to transfer ownership may not be
							exception-safe.  The release_safe() function is preferred.
			*/
			array_ptr_vector< T, ArrayAllocClass > release( iterator first, iterator last )
			{
				array_ptr_vector< T > newvector( first, last );
				v.erase( first.iter, last.iter );
				return newvector;
			}

			///	Removes and releases ownership of all the arrays in the collection.
			/**	The arrays are removed from the collection, and an array_ptr_vector
				containing pointers to them is returned.
				
				\warning	Using this member function to transfer ownership may not be
							exception-safe.  The release_safe() function is preferred.
			*/
			array_ptr_vector< T, ArrayAllocClass > release( void )
			{
				array_ptr_vector< T > newvector( *this );
				v.clear();
				return newvector;
			}

			///	Removes and releases ownership of a specified array in the collection in an exception-safe manner.
			/**	The array is removed from the collection, and the pointer to the array
				is returned.  The pointer is wrapped in a std::auto_ptr to ensure exception 
				safety.
			*/
			std::auto_ptr< T > release_safe( iterator position )
			{
				std::auto_ptr< T > tmp = position.get_ptr();
				v.erase( position.iter );
				return tmp;
			}

			///	Removes and releases ownership of a subset of arrays in the collection in an exception-safe manner.
			/**	This function is part of the exception-safe mechanism for transferring
				arrays between owned_array_ptr_vector instances, where the transfer() member
				function cannot be used (i.e. when the two instances are in different scopes,
				or when one/both instances are protected members of classes).  The arrays
				are removed from the collection, and a special heap-allocated owned_array_ptr_vector
				is returned, wrapped in a std::auto_ptr, to contain the objects.  The
				std::auto_ptr can be passed by value between functions, preserving the
				collection of heap-allocated arrays.  Eventually these can be added
				to another owned_array_ptr_vector using the constructor, assign_takeownership()
				or insert_takeownership() member functions.
			*/
			std::auto_ptr< owned_array_ptr_vector< T, ArrayAllocClass > > release_safe( iterator first, iterator last )
			{
				std::auto_ptr< owned_array_ptr_vector< T > > newvector( new owned_array_ptr_vector< T >() );
				newvector->transfer( newvector->begin(), first, last, *this );
				return newvector;
			}

			///	Removes and releases ownership of all the arrays in the collection in an exception-safe manner.
			/**	This function is part of the exception-safe mechanism for transferring
				arrays between owned_array_ptr_vector instances, where the transfer() member
				function cannot be used (i.e. when the two instances are in different scopes,
				or when one/both instances are protected members of classes).  The arrays
				are removed from the collection, and a special heap-allocated owned_array_ptr_vector
				is returned, wrapped in a std::auto_ptr, to contain the objects.  The
				std::auto_ptr can be passed by value between functions, preserving the
				collection of heap-allocated arrays.  Eventually these can be added
				to another owned_array_ptr_vector using the constructor, assign_takeownership()
				or insert_takeownership() member functions.
			*/
			std::auto_ptr< owned_array_ptr_vector< T, ArrayAllocClass > > release_safe( void )
			{
				std::auto_ptr< owned_array_ptr_vector< T > > newvector( new owned_array_ptr_vector< T >() );
				newvector->swap( *this );
				return newvector;
			}

			///	Removes and deallocates all arrays from the collection.
			void clear( void )
			{
				for( typename std::vector< T * >::iterator i = v.begin(); i < v.end(); i++ )
					alloc.dealloc( *i );
				v.clear();
			}

			///	Returns a reference to the resident instance of the array allocator class.
			ArrayAllocClass &get_alloc( void )
			{
				return this->alloc;
			}

		private:
			owned_array_ptr_vector( const owned_array_ptr_vector< T, ArrayAllocClass > &x );
			owned_array_ptr_vector< T, ArrayAllocClass > &operator=( const array_ptr_vector< T, ArrayAllocClass > &x );
			owned_array_ptr_vector< T, ArrayAllocClass > &operator=( const owned_array_ptr_vector< T, ArrayAllocClass > &x );

			std::vector< T * > v;
			ArrayAllocClass alloc;
	};

	/**	\name	Mathematical Functions and Constants
	*/

	//@{
	const double pi = 3.1415926535897932384626433832795;
	const float pi_f = 3.1415926535897932384626433832795F;

	///	Converts angles in degrees to radians.
	template< typename T > inline T ToRadians( T AngleDegrees ) {  return ( AngleDegrees / ( T ) 360.0 ) * ( T ) 6.283185307179586476925286766559;  }
	///	Converts angles in radians to degrees.
	template< typename T > inline T ToDegrees( T AngleRadians ) {  return ( AngleRadians / ( T ) 6.283185307179586476925286766559 ) * ( T ) 360.0;  }

	///	Computes the factorial of a number.
	/**	
		\warning	This function is suitable only for small values of n.  Depending
					on the type T, it can overflow for even very small input values.
	*/
	template< typename T > inline T Factorial( T n )
	{
		T out = n;
		for( T i = n - ( T ) 1; i > 1; i-- )
			out *= i;
		return out;
	}


	///	Computes the number of unique combinations of k elements obtainable from a set of n elements.
	/**	\warning	Because this function relies on a naive method for calculating
					the factorial, it is not useable with large n or k.  In fact, it
					may give erroneous results even for small n or k, depending on the
					type T.

		Example:  
		
		Given a set of 6 elements ABCDEF, how many unique combinations of 2 elements are possible?

		Cnk( 6, 2 ) = 6! / ( 2! * 4! ) = 15.  
		These combinations are AB, AC, AD, AE, AF, BC, BD, BE, BF, CD, CE, CF, DE, DF, EF.
	*/
	template< typename T > inline T Cnk( T n, T k ) {  return Factorial( n ) / ( Factorial( k ) * Factorial( n - k ) );  }


	///	Computes the sinc or "sine cardinal" function.
	/**	(see Bracewell, R.N.  The Fourier Transform and its Applications.  Boston:  McGraw-Hill, 2000.  3rd ed.)
	*/
	template< typename T > inline T sinc( T x )
	{  return ( T )( x == 0 ? 1 : 
	( sin( 3.1415926535897932384626433832795 * ( double ) x ) / ( 3.1415926535897932384626433832795 * ( double ) x ) ) );  }

	///	Computes the square of the sinc function.
	template< typename T > inline T sinc_sq( T x ) {  return sinc( x ) * sinc( x );  }

	///	Calculates powers of integers.
	/**	This function is intended to supply integer powers of integer values quickly.  
		It should inline to give a simple series of multiplication instructions.
	*/
	template< typename T > inline T intpow( T integer, T exponent )
	{
		T out = 1;
		for( int i = 0; i < exponent; i++ ) out *= integer;
		return out;
	}

	///	Returns the absolute value of a number.
	/**	This simple function gives the absolute value of a number.  It is designed to inline
		automatically, which would make it faster than calls to the standard library functions.
	*/
	template< typename T > inline T fast_abs( T x )
	{
		return x < ( T ) 0.0 ? ( ( T ) -1.0 ) * x : x;
	}


	///	Returns the square of a number.
	/**	This simple function gives the square of a number.  It is designed to inline automatically,
		which would make it faster than calls to the standard library functions.
	*/
	template< typename T > inline T fast_sqr( T x )
	{
		return x * x;
	}

	///	Tests whether a numerical value is valid.
	/**	This function can be used to test floating point values for validity.  It checks against both
		quiet and signalling NaN values, as well as positive and negative infinity.
	*/
	template< typename T > inline bool badval( T x )
	{
		if( x == std::numeric_limits< T >::quiet_NaN() || x == std::numeric_limits< T >::signaling_NaN()
			|| x == std::numeric_limits< T >::infinity() || x == -std::numeric_limits< T >::infinity() )
			return true;
		else return false;
	}

	template< typename T > inline int closest_int( T x )
	{
		if( x < ( T ) 0.0 ) return ( int ) ( x - 0.5 );
		else return ( int ) ( x + 0.5 );
	}

	//@}

	///	Parses a string into tokens.
	/**	This class borrows some features from the old C library function strtok(), and others
		come from more modern tokenizing libraries.    
		
		The string is split	on the basis of delimiters, which are the characters that divide the 
		tokens.  Delimiters	can be "included" or "excluded."  The excluded delimiters are the 
		more familiar kind:  They divide tokens, but they themselves do not wind up in the 
		output.  The included delimiters behave differently, in that each included delimiter 
		becomes a token of its own in the output.  Typically, white space characters would be 
		excluded delimiters, but operators or other symbols would be included delimiters.

		The parsing can be performed either all at once, or piecemeal.  The all-at-once method
		is the more straightforward, and should be the normal choice.  To do this, one calls 
		the member function process_full_string(), and a std::vector of tokens is returned.
		The other method is useful, though, when the delimiters for some parts of the string
		need to change from those of other parts, or when the delimiters must be chosen on
		the basis of the first tokens.  Most tokenizers don't allow for such circumstances,
		but this is occasionally very important, and it was one of the features of the old
		strtok().  To use it here, one calls process_next_token() to get the next token, then
		changes the delimiters if necessary, and then calls it again until the string is done.
		After each call, it is wise to call at_end() to check whether the last token has been
		read.  If after reading the first token(s) it becomes clear that the rest of the
		string can be parsed with the same delimiters, the remaining tokens can be collected
		with the simple process_remaining_string().

		Changing the delimiters between tokens is simply a matter of changing the public
		member variables delimiters_include and delimiters_exclude.

		To reuse an instance of str_tok, call reset() to load in a new string and clear the
		internal state.

		Although this class will accept C null-terminated strings as input, it produces all of
		its output as C++ std::strings and std::vectors of std::strings.
	*/
	class str_tok
	{
		public:
			///	Constructs a new str_tok with the supplied parameters.
			str_tok( std::string new_str = std::string( "" ), 
				std::string new_delimiters_exclude = std::string( " \t\f\r\n" ), std::string new_delimiters_include = std::string( "" ) ) 
				: str( new_str ), delimiters_exclude( new_delimiters_exclude ), delimiters_include( new_delimiters_include ), current( 0 ) {  }
			///	Constructs a new str_tok with the supplied parameters.
			str_tok( const char *new_str, const char *new_delimiters_exclude = " \t\f\r\n", const char *new_delimiters_include = "" ) 
				: str( new_str ), delimiters_exclude( new_delimiters_exclude ), delimiters_include( new_delimiters_include ), current( 0 ) {  }

			///	Parses the full submitted string and returns a collection of all the tokens found.
			std::vector< std::string > process_full_string( void )
			{
				std::string delimiters = delimiters_exclude + delimiters_include;
				for( std::string::size_type i = 0; i < str.length(); )
				{
					if( delimiters_include.find_first_of( str[ i ] ) != delimiters_include.npos )	//	if first character is an included delimiter
					{
						tokens.push_back( str.substr( i, 1 ) );										//	add it as a token
						i++;																//	advance so that the next character will be tested
						continue;
					}
					std::string::size_type pos = str.find_first_of( delimiters, i );				//	find next delimiter
					if( pos != str.npos )															//	if there is a delimiter
					{
						if( pos != i ) tokens.push_back( str.substr( i, pos - i ) );	//	if there is text before the next delim - add as a token
						if( delimiters_exclude.find_first_of( str[ pos ] ) != delimiters_exclude.npos )		//	is the next delimiter to be a token?	
						{
							i = str.find_first_not_of( delimiters_exclude, pos );					//	not a token - skip the delimiter
							continue;
						}
						i = pos;																	//	should be token - set next round for this pos
						continue;
					}
					else																	//	no delimiters - the rest of the string is one token
					{
						tokens.push_back( str.substr( i ) );
						break;
					}
				}
				current = str.length();
				return tokens;
			}

			///	Parses the portion of the string not already parsed, and returns the tokens found.
			std::vector< std::string > process_remaining_string( void )
			{
				std::vector< std::string > newtokens;
				std::string delimiters = delimiters_exclude + delimiters_include;
				for( std::string::size_type i = current; i < str.length(); )
				{
					if( delimiters_include.find_first_of( str[ i ] ) != delimiters_include.npos )
					{
						newtokens.push_back( str.substr( i, 1 ) );
						i++;
						continue;
					}
					std::string::size_type pos = str.find_first_of( delimiters, i );
					if( pos != str.npos )
					{
						if( pos != i ) newtokens.push_back( str.substr( i, pos - i ) );
						if( delimiters_exclude.find_first_of( str[ pos ] ) != delimiters_exclude.npos )
						{
							i = str.find_first_not_of( delimiters_exclude, pos );
							continue;
						}
						i = pos;
						continue;
					}
					else
					{
						newtokens.push_back( str.substr( i ) );
						break;
					}
				}
				current = str.length();
				std::copy( newtokens.begin(),newtokens.end(), std::back_inserter( tokens ) );
				return newtokens;
			}

			///	Finds and returns the next token in the string.
			/**	This function is intended to be used repeatedly, to return one token each time,
				until the whole string is processed.  The delimiters may be changed between calls
				to this function.  Tokens returned in previous calls to this function are also
				recorded in the tokens member variable.  It is advisable to call at_end()
				before calling this function, to test for whether there are more tokens to
				return.  Calling this function after all tokens have been returned will cause
				it to return empty strings.  After beginning processing of a string
				using this function, the remainder may be processed in one shot using the
				process_remaining_string() member function.

				\warning	Do NOT loop on the results of this function without testing at_end()
							before each iteration.
			*/
			std::string process_next_token( void )
			{
				std::string delimiters = delimiters_exclude + delimiters_include, out;
				if( current >= str.length() ) return std::string( "" );

				if( delimiters_include.find_first_of( str[ current ] ) != delimiters_include.npos )
				{
					out = str.substr( current, 1 );
					current++;
				}
				std::string::size_type pos = str.find_first_of( delimiters, current );
				if( pos != str.npos )
				{
					if( pos != current ) out = str.substr( current, pos - current );
					if( delimiters_exclude.find_first_of( str[ pos ] ) != delimiters_exclude.npos )
						current = str.find_first_not_of( delimiters_exclude, pos );
					else
						current = pos;
				}
				else
				{
					out = str.substr( current );
					current = str.length();
				}
				tokens.push_back( out );
				return out;
			}

			///	Tests whether the last token has been returned.
			bool at_end( void )
			{
				if( current >= str.length() ) return true;
				else return false;
			}

			///	Clears the state of the object so that a new string can be processed.
			void reset( void )
			{
				str = "";
				current = 0;
				tokens.clear();
			}

			///	Clears the state of the object and accepts a new string to be processed.
			void reset( std::string new_str )
			{
				str = new_str;
				current = 0;
				tokens.clear();
			}

			///	Clears the state of the object and accepts a new string to be processed.
			void reset( std::string new_str, std::string new_delimiters_exclude, std::string new_delimiters_include )
			{
				str = new_str;
				delimiters_exclude = new_delimiters_exclude;
				delimiters_include = new_delimiters_include;
				current = 0;
				tokens.clear();
			}

			///	The string to be processed.  This must not be altered once parsing has begun, without first calling reset().
			std::string str;
			///	A collection containing all of the tokens obtained from the current string.
			std::vector< std::string > tokens;
			///	The characters to serve as "included delimiters," which divide text tokens and which themselves are included in the output as tokens.
			std::string delimiters_include;
			///	The characters to serve as "excluded delimiters," which divide text tokens but are not included in the output.
			std::string delimiters_exclude;

		private:
			std::string::size_type current;
	};

	///	Parses strings, after the C library function strtok().
	/**	StrTok is a simple string parser, which basically implements the functionality
		of the C library function strtok.  It parses a string one token at a time,
		and the token delimiters can be changed as the tokens are parsed.  
		
		One can either supply a string on creation, or set a string later using 
		SetString().  The first token is obtained by calling Next(), and additional
		tokens are obtained via subsequent calls to Next().  The last token read
		can always be obtained by calling Last().  The delimiters for parsing
		future tokens are set by calling SetDelimiters() or by including new
		delimiters in a call to Next().  AtEnd() tests whether the current token
		is the last in the string.

		\deprecated	This class has been superceded by the new str_tok class, also
					implemented in this header file.  The str_tok class is a modern
					C++ incarnation that has a simpler and more straightforward
					interface.  Moreover, the str_tok interface is generally not
					suceptible to the null pointer errors that can be encountered
					easily with StrTok if it is not used carefully.  While StrTok
					carefully models the behavior of the ancient strtok() library
					function, there are better approaches for most cases.
					Nonetheless, this class will likely be included in this header
					indefinitely, due to the dependencies of existing code, but no
					future development work will be undertaken on or with it.
	*/
	class StrTok
	{
		public:
			///	Constructs a StrTok parser.
			StrTok( void ): str( 0 ), current( 0 ), next( 0 ), end( 0 ), Delimiters( 0 )
			{
				SetDelimiters( " \t\f\r\n" );
			}

			///	Constructs a StrTok parser, and supplies a string to be parsed.
			StrTok( char *String ) : str( 0 ), current( 0 ), next( 0 ), end( 0 ), Delimiters( 0 )
			{
				SetString( String );
				SetDelimiters( " \t\f\r\n" );
			}

			///	Constructs a StrTok parser, and supplies both a string to be parsed and an initial set of delimiters.
			StrTok( char *String, char *DefaultDelimiters ) : str( 0 ), current( 0 ), next( 0 ), end( 0 ), Delimiters( 0 )
			{
				SetString( String );
				SetDelimiters( DefaultDelimiters );
			}

			~StrTok( void )
			{
				if( Delimiters ) delete [] Delimiters;
				if( str ) delete [] str;
			}

			///	Sets the string to be parsed.  The previous string and tokens, if any, are cleared.
			void SetString( char *String )
			{
				if( !String ) return;
				if( str ) delete [] str;
				str = new char[ strlen( String ) + 1 ];
				strcpy( str, String );
				next = str;
				end = str + strlen( str );
				current = 0;
			}

			///	Sets the delimiters used when parsing future tokens.
			void SetDelimiters( char *NewDelimiters )
			{
				if( !NewDelimiters ) return;
				if( Delimiters ) delete [] Delimiters;
				Delimiters = new char[ strlen( NewDelimiters ) + 1 ];
				strcpy( Delimiters, NewDelimiters );
			}

			///	Obtains the next token, or returns a null pointer if no more tokens are available.
			char *Next( void )
			{
				if( !next ) return 0;
				if( next == str ) while( next && next != end && next == strpbrk( next, Delimiters ) ) *( next++ ) = 0;
				if( next == end ) return 0;
				if( next == strpbrk( next, Delimiters ) ) while( next && next != end && next == strpbrk( next, Delimiters ) ) *( next++ ) = 0;
				current = next;
				next = strpbrk( next, Delimiters );
				if( next )
				{
					*( next++ ) = 0;
					while( next && next != end && next == strpbrk( next, Delimiters ) ) *( next++ ) = 0;
				}
				return current;
			}

			///	Obtains the next token based on new delimiters, or returns a null pointer if no more tokens are available.
			char *Next( char *NewDelimiters )
			{
				SetDelimiters( NewDelimiters );
				return Next();
			}

			///	Returns a pointer to the last token obtained previously.
			char *Last( void )	
			{
				return current;
			}

			///	Tests whether or not the current token is the last.
			bool AtEnd( void )
			{
				if( !next || next == end )
					return true;
				else if( next == str )
				{
					char *test = next;
					while( test && test != end && test == strpbrk( test, Delimiters ) ) test++;
					if( test == end ) return true;
					else return false;
				}
				else return false;
			}

			char *Delimiters;

		private:
			char *str;
			char *current;
			char *next;
			char *end;
	};


	///	Temporary container for a heap-allocated string.
	/**	This class is designed to hold a heap-allocated string temporarily, to ensure
		that it is deleted correctly when it is no longer needed.  It is especially
		ideal for use as a <em>temporary object</em> within an expression, for passing
		a string between functions.  It converts to a char* automatically, so it can
		be used with any code expecting a char* with no further modification.  It
		deletes the string that it owns when it goes out of scope.

		As an example, consider a procedure src() that returns a heap-allocated string,
		and a procedure dest() that processes that string.  Without tmpstr, one would
		write:

		char *str = src();
		dest( str );
		delete str;

		But with tmpstr, this is written in one line as:

		dest( tmpstr( src() ) );

		The call to src() returns a char*, which is wrapped in the tmpstr.  The tmpstr
		converts automatically to a char*, which is passed to dest().  However, the tmpstr
		lives on as a temporary object, which is not deleted until the evaluation of the
		expression is completed.  At that point, upon the return of dest(), the tmpstr
		deletes the string.
	*/
	class tmpstr
	{
		public:
			tmpstr( char *instr )
			{
				str = instr;
			}

			~tmpstr( void )
			{
				delete [] str;
			}

			operator char *( void )
			{
				return str;
			}

		private:
			tmpstr( void );
			tmpstr( tmpstr &Source );

			tmpstr &operator=( tmpstr &Source );

			char *str;
	};


	template< typename T >
	inline std::string format_value( T value )
	{
		std::ostringstream output;
		output << value;
		return output.str();
	}


	///	STL-compatible predicate testing whether the absolute value of parameter x is less than that of parameter y.
	template< typename T > class abs_less
	{
		public:
			bool operator() ( const T &x, const T &y )
			{
				T a = x < 0 ? x * ( T ) -1.0 : x;
				T b = y < 0 ? y * ( T ) -1.0 : y;
				return a < b;
			}
	};

	///	STL-compatible predicate testing whether the absolute value of parameter x is greater than that of parameter y.
	template< typename T > class abs_greater
	{
		public:
			bool operator() ( const T &x, const T &y )
			{
				T a = x < 0 ? x * ( T ) -1.0 : x;
				T b = y < 0 ? y * ( T ) -1.0 : y;
				return a > b;
			}
	};

	///	STL-compatible predicate testing whether the first element of the std::pair x is greater than that of the std::pair y.
	template< typename T > class pair_first_less
	{
		public:
			bool operator() ( const T &x, const T &y )
			{
				return x.first < y.first;
			}
	};

	///	STL-compatible predicate testing whether the second element of the std::pair x is greater than that of the std::pair y.
	template< typename T > class pair_second_less
	{
		public:
			bool operator() ( const T &x, const T &y )
			{
				return x.second < y.second;
			}
	};

	///	STL-compatible predicate testing whether the first element of the std::pair x is greater than that of the std::pair y.
	template< typename T > class pair_first_greater
	{
		public:
			bool operator() ( const T &x, const T &y )
			{
				return x.first > y.first;
			}
	};

	///	STL-compatible predicate testing whether the second element of the std::pair x is greater than that of the std::pair y.
	template< typename T > class pair_second_greater
	{
		public:
			bool operator() ( const T &x, const T &y )
			{
				return x.second > y.second;
			}
	};

	///	STL-style binder, returns a constant whenever called.
	template< typename ConstantType > class binder_constant
	{
		public:
			binder_constant( ConstantType Value ) : value( Value ) {  }

			ConstantType operator() ( void )
			{
				return this->value;
			}

		private:
			const ConstantType value;
	};


	///	Creates a functor that returns a constant value, in the fashion of the STL binder objects.
	template< typename ConstantType >
	binder_constant< ConstantType > bind_constant( ConstantType Value )
	{
		return binder_constant< ConstantType >( Value );
	}


	///	STL-style binder that pretends to be a unary function, but returns a constant whenever called.
	template< typename ConstantType, typename UnaryArgumentType = ConstantType > 
	class binder_constant_as_unary : public std::unary_function< UnaryArgumentType, ConstantType >
	{
		public:
			binder_constant_as_unary( ConstantType Value ) : value( Value ) {  }

			ConstantType operator() ( const UnaryArgumentType Param )
			{
				return this->value;
			}

		private:
			const ConstantType value;
	};


	///	Creates a functor that acts like a unary function but returns a constant value, in the fashion of the STL binder objects.
	template< typename ConstantType >
	binder_constant_as_unary< ConstantType, ConstantType > bind_constant_as_unary( ConstantType Value )
	{
		return binder_constant_as_unary< ConstantType, ConstantType >( Value );
	}


	///	Creates a functor that acts like a unary function but returns a constant value, in the fashion of the STL binder objects.
	template< typename ConstantType, typename UnaryArgumentType >
	binder_constant_as_unary< ConstantType, UnaryArgumentType > bind_constant_as_unary( ConstantType Value )
	{
		return binder_constant_as_unary< ConstantType, UnaryArgumentType >( Value );
	}


	///	STL-style binder that pretends to be a binary function, but returns a constant whenever called.
	template< typename ConstantType, typename FirstArgumentType = ConstantType, typename SecondArgumentType = ConstantType > 
	class binder_constant_as_binary : public std::binary_function< FirstArgumentType, SecondArgumentType, ConstantType >
	{
		public:
			binder_constant_as_binary( ConstantType Value ) : value( Value ) {  }

			ConstantType operator() ( const FirstArgumentType Param1, const SecondArgumentType Param2 )
			{
				return this->value;
			}

		private:
			ConstantType value;
	};


	///	Creates a functor that acts like a binary function but returns a constant value, in the fashion of the STL binder objects.
	template< typename ConstantType >
	binder_constant_as_binary< ConstantType, ConstantType, ConstantType > bind_constant_as_binary( ConstantType Value )
	{
		return binder_constant_as_binary< ConstantType, ConstantType, ConstantType >( Value );
	}


	///	Creates a functor that acts like a binary function but returns a constant value, in the fashion of the STL binder objects.
	template< typename ConstantType, typename FirstArgumentType, typename SecondArgumentType >
	binder_constant_as_binary< ConstantType, FirstArgumentType, SecondArgumentType > bind_constant_as_binary( ConstantType Value )
	{
		return binder_constant_as_binary< ConstantType, FirstArgumentType, SecondArgumentType >( Value );
	}


	///	A very simple matrix implemented using valarrays
	template< typename T >
	struct vamatrix
	{
		public:
			vamatrix( int Rows, int Cols ) : cols( Cols ), rows( Rows ), data( ( T ) 0.0, Cols * Rows ) {  }

			vamatrix( const vamatrix< T > &src )
			{
				this->cols = src.cols;
				this->rows = src.rows;
				this->data = src.data;
			}

			vamatrix< T > &operator=( const vamatrix< T > &rhs )
			{
				this->cols = rhs.cols;
				this->rows = rhs.rows;
				this->data.resize( rhs.data.size() );
				this->data = rhs.data;

				return *this;
			}

			T &operator()( int row, int col )
			{
				return data[ row * cols + col ];
			}

			T operator()( int row, int col ) const
			{
				return data[ row * cols + col ];
			}

			std::valarray< T > data;
			int cols;
			int rows;

			struct IncompatibleMatricesException
			{
				const char *what( void )
				{
					return "The matrices are not compatible for the requested operation.";
				}
			};
	};


	template< typename T >
	vamatrix< T > MatrixProduct( const vamatrix< T > &a, const vamatrix< T > &b )
	{
		if( b.rows != a.cols ) throw typename vamatrix< T >::IncompatibleMatricesException();
		vamatrix< T > result( b.cols, a.rows );

		for( int out_row = 0; out_row < result.rows; out_row++ )
			for( int out_col = 0; out_col < result.cols; out_col++ )
				for( int index = 0; index < a.cols; index++ )
					result( out_row, out_col ) += a( out_row, index ) * b( index, out_col );

		return result;
	}

	///	Computes the tensor product of two matrices
	template< typename T >
	vamatrix< T > TensorProduct( const vamatrix< T > &a, const vamatrix< T > &b )
	{
		vamatrix< T > result( a.cols * b.cols, a.rows * b.rows );

		for( int ax = 0; ax < a.cols; ax++ )
			for( int ay = 0; ay < a.rows; ay++ )
				for( int bx = 0; bx < b.cols; bx++ )
					for( int by = 0; by < b.rows; by++ )
						result( ay * b.rows + by, ax * b.cols + bx ) = a( ay, ax ) * b( by, bx );

		return result;
	}


	///	Swaps the order of bytes in a data value.
	template< typename T >
	T SwapByteOrder( T Source )
	{
		T Dest;
		char *dest = ( char * ) &Dest;
		char *src = ( char * ) &Source;
		int size = sizeof( T );

		for( int x = 0, y = size - 1; x < size; x++, y-- )
			dest[ x ] = src[ y ];

		return Dest;
	}


#ifdef INCLUDE_KBRT
	//	KBRT:  Detects keypresses without blocking.
	//	This code requires facilities besides the ANSI C/ISO C++ standard libraries, so
	//	it is protected inside ifdef guards.  If you need it, define INCLUDE_KBRT, and
	//	make sure that you are either on Win32 or POSIX.
	//
	//	To use the class:  Create the object at the point in the program when you want
	//	to begin monitoring the keyboard.  On Windows it won't need to do anything
	//	special, but on POSIX it will need to switch the terminal mode.  Note that you
	//	will lose keyboard echoing at this point.  It will then monitor the keyboard
	//	until you destroy the object, at which point it will restore the terminal.
	//	To check for a keypress, call KBHit.  If this is true, you can use GetCh to
	//	return the character.  Note that GetCh may block if there isn't a character in
	//	the buffer - so if you don't want it to block, call KBHit first to make sure
	//	there's something there, as KBHit is nonblocking.

	///	Detects keypresses without blocking
	/**	This class allows one to detect keypresses without blocking.  It relies on 
		features of the Win32 or POSIX libraries which are not available in ANSI C
		or ISO C++, and therefore is <em>not</em> included by default.  To include it,
		define the preprocessor macro INCLUDE_KBRT, and make sure that either Win32
		or POSIX is available.

		To use the class:  Create the object at the point in the program when you want
		to begin monitoring the keyboard.  On Windows it won't need to do anything
		special, but on POSIX it will need to switch the terminal mode.  Note that you
		will lose keyboard echoing at this point.  It will then monitor the keyboard
		until you destroy the object, at which point it will restore the terminal.
		To check for a keypress, call KBHit().  If this is true, you can use GetCh() to
		return the character.  Note that GetCh() may block if there isn't a character in
		the buffer - so if you don't want it to block, call KBHit() first to make sure
		there's something there, as KBHit() is nonblocking.

	*/
	class KBRT
	{
#ifndef WIN32
		termios InitialSettings, NewSettings;
		unsigned char ch;
		bool hit;
#endif

		public:
			///	Constructs a KBRT and begins monitoring the keyboard.
			KBRT( void )
			{
#ifndef WIN32
				tcgetattr( 0, &InitialSettings);
				NewSettings = InitialSettings;
				NewSettings.c_lflag &= ~ICANON;
				NewSettings.c_lflag &= ~ECHO;
				NewSettings.c_cc[ VMIN ] = 0;
				NewSettings.c_cc[ VTIME ] = 0;
				tcsetattr( 0, TCSANOW, &NewSettings );
#endif
			}

			~KBRT( void )
			{
#ifndef WIN32
				tcsetattr( 0, TCSANOW, &InitialSettings );
#endif
			}
	
			///	Tests for whether a key has been pressed.
			bool KBHit( void )
			{
#ifdef WIN32
				if( _kbhit() ) return true;
				else return false;
#else
				if( hit ) return true;
				else if( read( 0, &ch, 1 ) )
				{
					hit = true;
					return true;
				}
				return false;
#endif
			}

			///	Returns the code of the key for the next keypress waiting in the key buffer.
			/**	This function may block if the keyboard has not been pressed.  Call
				KBHit() first to test for keys in the key buffer.
			*/
			int GetCh( void )
			{
#ifdef WIN32
				return _getch();
#else
				if( hit )
				{
					hit = false;
					return ( int ) ch;
				}
				else
				{
					NewSettings.c_cc[ VMIN ] = 1;
					tcsetattr( 0, TCSANOW, &NewSettings );
					if( read( 0, &ch, 1 ) ) 
					{
						NewSettings.c_cc[ VMIN ] = 0;
						tcsetattr( 0, TCSANOW, &NewSettings );
						return ( int ) ch;
					}
					NewSettings.c_cc[ VMIN ] = 0;
					tcsetattr( 0, TCSANOW, &NewSettings );
					return 0;
				}
#endif
			}
	};

#endif		// INCLUDE_KBRT

	///	Deallocates objects by calling the scalar delete operator.
	/**	This class is used by the smart pointer classes	to manage object deletion.  
		It is the default if another deallocator class is not specified.
	*/
	template< typename T >
	struct DefaultObjectDeallocator
	{
		void dealloc( T *ptr )
		{
			delete ptr;
		}
	};

	///	Smart pointer deallocator which does not necessarily delete the object referenced.
	/**	This class serves a counterintuitive purpose, allowing one to store an
		object in a smart pointer or a pointer collection, without the pointer
		necessarily being subject to automatic deletion when removed.  The idea is
		that this permits one to use a smart pointer in a case where automatic
		deletion is sometimes, but not always, needed.  Consider, for example, a case
		where a function call returns a pointer to a polymorphic object that is sometimes
		held externally, and that sometimes is to be held by the receiver.  The
		return value of the function could be stored in a stack_ptr with this type
		of deallocator, and a reference to the deallocator could be kept on the stack.
		The owner member of the deallocator can then be changed as needed, depending on
		the ownership situation with the received object.

		The owner member designates whether or not the smart pointer/pointer container is
		considered to own its object(s).  If owner == true, the deallocator deletes; if
		false, it does not.
	*/
	template< typename T >
	struct ObjectDeallocatorOptionalDelete
	{
		ObjectDeallocatorOptionalDelete( void ) : owner( true ) {  }
		ObjectDeallocatorOptionalDelete( bool initialsetting ) : owner( initialsetting ) {  }

		void dealloc( T *ptr )
		{
			if( owner ) delete ptr;
		}

		bool owner;
	};

	///	Deallocates array by calling the array delete operator.
	/**	This class is used by the owned_array_ptr_vector class to manage object deletion.  
		It is the default if another deallocator class is not specified.
	*/
	template< typename T >
	struct DefaultArrayDeallocator
	{
		void dealloc( T *ptr )
		{
			delete [] ptr;
		}
	};

	///	Smart pointer deallocator which does not necessarily delete the array referenced.
	/**	This class serves a counterintuitive purpose, allowing one to store an
		array in a smart pointer or a pointer collection, without the pointer
		necessarily being subject to automatic deletion when removed.  The idea is
		that this permits one to use a smart pointer in a case where automatic
		deletion is sometimes, but not always, needed.  Consider, for example, a case
		where a function call returns a pointer to an array that is sometimes
		held externally, and that sometimes is to be held by the receiver.  The
		return value of the function could be stored in a stack_array_ptr with this type
		of deallocator, and a reference to the deallocator could be kept on the stack.
		The owner member of the deallocator can then be changed as needed, depending on
		the ownership situation with the received object.

		The owner member designates whether or not the smart pointer/pointer container is
		considered to own its object(s).  If owner == true, the deallocator deletes; if
		false, it does not.
	*/
	template< typename T >
	struct ArrayDeallocatorOptionalDelete
	{
		ArrayDeallocatorOptionalDelete( void ) : owner( true ) {  }
		ArrayDeallocatorOptionalDelete( bool initialsetting ) : owner( initialsetting ) {  }

		void dealloc( T *ptr )
		{
			if( owner ) delete [] ptr;
		}

		bool owner;
	};

	///	The default object factory for owned_ptr_vector instances.
	/**	This class is used by the owned_ptr_vector to manage object creation, cloning
		and deallocation.  It is the default if another factory class is not specified.
		
		When asked to generate a new object of type T as a clone of an existing object
		of type T held by reference, this class invokes the new operator, supplying a 
		pointer to the original	object as a single constructor argument.

		When asked to generate an object of type T from a reference to an object/value of
		some other type X, this class invokes the new operator, supplying the object of
		type X as the constructor argument.  This should automatically invoke the copy
		constructor of type T if X == T, and otherwise it should invoke a conversion
		constructor for type X if one is available.

		Deallocation is performed with the delete operator.
	*/
	template< typename T >
	struct DefaultObjectFactory
	{
		T *operator()( const T *src )
		{
			return new T( *src );
		}

		template< typename X >
		T *operator()( const X &src )
		{
			return new T( src );
		}

		void dealloc( T *ptr )
		{
			delete ptr;
		}
	};

	///	The default array allocator for owned_array_ptr_vector instances.
	/**	This class is used by the owned_array_ptr_vector to manage array creation
		and deallocation.  It is the default if another allocator class is not 
		specified.  It creates new arrays using the new [] operator, and deletes arrays
		using them using the delete [] operator.  No initialization is performed
		on new arrays.
	*/
	template< typename T > 
	class DefaultArrayAlloc
	{
		public:
			T *alloc( size_t arraysize )
			{
				return new T[ arraysize ];
			}

			void dealloc( T *ptr )
			{
				delete [] ptr;
			}
	};

	///	Class implementing a simple agglomerative clustering algorithm.
	/**	This class implements a simple agglomerative clustering algorithm,
		combining data elements that are within a certain threshold of one
		another.  Supply a vector of data elements, a class that is able
		to calculate the distances between elements, and a threshold; the
		result is a vector of vectors, with each inner vector defining a
		cluster.

		\param	input		Vector of input data to be clustered.
		\param	threshold	Cutoff distance to assign data elements to the same cluster.
		\param	dist_calc	Functor to calculate distances between individual
							data points as well as already-formed clusters.
							The functor should accept two vectors of data
							points as arguments to its () operator.  These
							may each contain a single element, or they may
							contain sets of elements that have already been
							merged to make clusters.  In the latter case,
							the functor should follow a distance metric to 
							assign a distance between a single data point 
							and a cluster of points, or	between two 
							clusters of points.
	*/
	template< typename DataObjClass, typename DataDistanceCalcClass >
	class cluster_data
	{
	public:
		std::vector< std::vector< DataObjClass > > operator()( std::vector< DataObjClass > input, float threshold, DataDistanceCalcClass dist_calc = DataDistanceCalcClass() )
		{
			clusters.clear();
			for( typename std::vector< DataObjClass >::iterator i = input.begin(); i != input.end(); ++i )
			{
				std::vector< DataObjClass > c;
				c.push_back( *i );
				clusters.push_back( c );
			}

			while( 1 )
			{
				int n = clusters.size();
				int dist_matrix_size = calc_total_matrix_entries( n );
				std::vector< float > dist_matrix( dist_matrix_size );
				for( int row = 0; row < n; row++ )
					for( int col = row + 1; col < n; col++ )
					{
						int offset = calc_matrix_offset( row, col, n );
						dist_matrix[ offset ] = dist_calc( clusters[ row ], clusters[ col ] );
					}

				int min_dist_row = 0, min_dist_col = 0;
				float min_dist = -1.0F;
				for( int row = 0; row < n; row++ )
					for( int col = row + 1; col < n; col++ )
					{
						int offset = calc_matrix_offset( row, col, n );
						if( min_dist < 0.0F || dist_matrix[ offset ] < min_dist )
						{
							min_dist_row = row;
							min_dist_col = col;
							min_dist = dist_matrix[ offset ];
						}
					}

				if( min_dist > 0.0F && min_dist < threshold )
				{
					std::vector< DataObjClass > c1 = clusters[ min_dist_row ];
					std::vector< DataObjClass > c2 = clusters[ min_dist_col ];

					std::copy( c2.begin(), c2.end(), back_inserter( c1 ) );

					clusters.erase( clusters.begin() + min_dist_col );
				}
				else break;
			}
		}

		std::vector< std::vector< DataObjClass > > clusters;

	private:
		int calc_total_matrix_entries( int n )
		{
			return round( ( ( float ) n ) * ( ( float ) ( n + 1 ) ) * 0.5F );
		}

		int calc_matrix_offset( int row, int col, int n )
		{
			int offset_prev_rows = round( ( ( float ) row ) * ( ( float ) ( row + 1 - 2 * n ) ) * ( -0.5F ) );
			return offset_prev_rows + col - row - 1;
		}
	};


#ifdef INCLUDE_JOB_QUEUE

}	//	temporarily exit the namespace so we can include boost files

#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

namespace BEC_Misc
{

	template< typename JobClass, typename WorkerThreadClass >
	class job_queue
	{
	public:
		job_queue( void ) : worker_thread_pattern( WorkerThreadClass() ), jobs_completed( 0 ), total_jobs( 0 ), shutdown( false ) {  }

		job_queue( int NumOfWorkers ) : worker_thread_pattern( WorkerThreadClass() ), jobs_completed( 0 ), total_jobs( 0 ), shutdown( false )
		{
			for( int i = 0; i < NumOfWorkers; i++ )
			{
				worker_thread new_worker_thread( *this, WorkerThreadClass() );
				std::auto_ptr< boost::function0< void > > new_thread( new boost::function0< void >( new_worker_thread ) );
				this->threads.create_thread( new_thread.get() );
				this->thread_functors.push_back( new_thread.release() );
			}
		}

		job_queue( int NumOfWorkers, WorkerThreadClass WorkerThreadPattern ) : worker_thread_pattern( WorkerThreadPattern ), 
			jobs_completed( 0 ), total_jobs( 0 ), shutdown( false )
		{
			for( int i = 0; i < NumOfWorkers; i++ )
			{
				worker_thread new_worker_thread( *this, this->worker_thread_pattern );
				std::auto_ptr< boost::function0< void > > new_thread( new boost::function0< void >( new_worker_thread ) );
				this->threads.create_thread( *( new_thread.get() ) );
				this->thread_functors.push_back( new_thread.release() );
			}
		}

		~job_queue( void )
		{
			{
				boost::mutex::scoped_lock lock( this->mutex );
				this->shutdown = true;
				while( this->jobs_completed < this->total_jobs )
					this->jobs_done.wait( lock );
			}
			this->activate_workers.notify_all();
			this->threads.join_all();
		}

		void add_worker( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			worker_thread new_worker_thread( *this, this->worker_thread_pattern );
			std::auto_ptr< boost::function0< void > > new_thread( new boost::function0< void >( new_worker_thread ) );
			this->threads.create_thread( new_thread.get() );
			this->thread_functors.push_back( new_thread.release() );
		}

		int num_of_workers( void )
		{
			return this->thread_functors.size();
		}

		void add_job( JobClass Job )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			job_entry new_job( ++this->total_jobs, Job );
			this->pending.insert( this->pending.end(), new_job );
			this->queue.push( new_job );
			this->activate_workers.notify_one();
		}
		
		bool empty( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			return this->queue.empty();
		}

		bool done( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			return this->jobs_completed == this->total_jobs;			
		}

		void wait_until_empty( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			while( !this->queue.empty() ) 
				this->queue_empty.wait( lock );
		}

		void wait_until_done( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			while( this->jobs_completed != this->total_jobs )
				this->jobs_done.wait( lock );
		}

		std::vector< JobClass > completed_jobs( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			std::vector< JobClass > out;
			for( typename std::map< size_t, JobClass >::iterator i = this->completed.begin(); i != this->completed.end(); ++i )
				out.push_back( i->second );
			return out;
		}

		std::vector< JobClass > active_jobs( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			std::vector< JobClass > out;
			for( typename std::map< size_t, JobClass >::iterator i = this->active.begin(); i != this->active.end(); ++i )
				out.push_back( i->second );
			return out;
		}

		std::vector< JobClass > pending_jobs( void )
		{
			boost::mutex::scoped_lock lock( this->mutex );

			std::vector< JobClass > out;
			for( typename std::map< size_t, JobClass >::iterator i = this->pending.begin(); i != this->pending.end(); ++i )
				out.push_back( i->second );
			return out;
		}

	private:
		typedef std::pair< size_t, JobClass > job_entry;

		struct worker_thread
		{
			worker_thread( job_queue &Parent, WorkerThreadClass UserWorkerObj )
				: parent( Parent ), user_worker_obj( UserWorkerObj ) {  }

			void operator()( void )
			{
				while( true )
				{
					typename job_queue::job_entry job;

					{
						boost::mutex::scoped_lock lock( this->parent.mutex );

						if( this->parent.shutdown ) break;
						while( this->parent.queue.empty() )
						{
							this->parent.activate_workers.wait( lock );
							if( this->parent.shutdown ) break;
						}
						if( this->parent.shutdown )
							break;

						job = this->parent.queue.front();
						this->parent.queue.pop();
						this->parent.pending.erase( job.first );
						this->parent.active.insert( job );

						if( this->parent.queue.empty() )
							this->parent.queue_empty.notify_all();
					}

					this->user_worker_obj( job.second );

					{
						boost::mutex::scoped_lock lock( this->parent.mutex );

						this->parent.active.erase( job.first );
						this->parent.completed.insert( job );
						this->parent.jobs_completed++;
						if( this->parent.jobs_completed == this->parent.total_jobs )
							this->parent.jobs_done.notify_all();
						if( this->parent.shutdown ) break;
					}
				}
			}

			job_queue &parent;
			WorkerThreadClass user_worker_obj;
		};
		friend struct worker_thread;

		size_t jobs_completed;
		size_t total_jobs;
		std::queue< job_entry > queue;
		std::map< size_t, JobClass > pending;
		std::map< size_t, JobClass > active;
		std::map< size_t, JobClass > completed;

		boost::thread_group threads;
		boost::mutex mutex;
		boost::condition queue_empty;
		boost::condition activate_workers;
		boost::condition jobs_done;
		bool shutdown;
		WorkerThreadClass worker_thread_pattern;
		owned_ptr_vector< boost::function0< void > > thread_functors;

		job_queue( const job_queue &src );
	};

#endif

}

#endif		// BEC_MISC_INCLUDED

/*  End becmisc.h  */

