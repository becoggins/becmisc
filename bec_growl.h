#ifndef BEC_GROWL_INCLUDED
#define BEC_GROWL_INCLUDED

#include <string>

namespace BEC_Growl
{
    namespace details
    {
        struct gn_delegate_owner;
    }
    
    class GrowlNotifier
    {
    public:
        GrowlNotifier( void );
        ~GrowlNotifier( void );
        void Notify( std::string Title, std::string Message, bool IsSticky = false ); 
        
    private:
        details::gn_delegate_owner *d_own;
        
    };
}

#endif
