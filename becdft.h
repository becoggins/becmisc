/*	becdft.h
	Brian E. Coggins, 14 Dec 2004, rev. 14 Dec 2004, v.0.0.1
	Copyright (c) 2004, Brian E. Coggins.  All rights reserved.

	Brian E. Coggins C++ Library
	Classes for Discrete Fourier Transforms

	To use this library in an application, include "becdft.h," and use the
	classes and templates as needed.  Please see the library documentation 
	for details.

*/

#ifndef BEC_DFT_INCLUDED
#define BEC_DFT_INCLUDED
#define BECDFT_LIBRARY_VERSION "0.1.1"
#define BECDFT_LIBRARY_VERSION_DATE "27 Oct 2004"
#define BECDFT_LIBRARY_COPYRIGHT "Copyright (c) 2004, Brian E. Coggins.  All rights reserved."

#include <math.h>
#include <memory.h>

#if defined( FFT_USE_MKL )
#include "mkl_dfti.h"
#elif defined( FFT_USE_FFTPACK )
extern "C" void rffti_( int *n, float *wsave );
extern "C" void rfftf_( int *n, float *data, float *wsave );
extern "C" void rfftb_( int *n, float *data, float *wsave );
#endif

namespace BEC_DFT
{
	class FFTReal
	{
		public:
			FFTReal() : a( 0 ), b( 0 ), c( 0 )  {  };
			FFTReal( int n ) : a( 0 ), b( 0 ), c( 0 )
			{
				this->Initialize( n );
			}
			~FFTReal()
			{
#if defined( FFT_USE_MKL )
				DftiFreeDescriptor( ( DFTI_DESCRIPTOR ** ) &a );
#elif defined( FFT_USE_FFTPACK )
				delete [] ( float * ) a;
				delete ( int * ) b;
#else
				return;
#endif
			}

			bool inline Initialize( int n )
			{
#if defined( FFT_USE_MKL )
				int status;
				status = DftiCreateDescriptor( ( DFTI_DESCRIPTOR ** ) &a, DFTI_SINGLE, DFTI_REAL, 1, n );
				status = DftiSetValue( ( DFTI_DESCRIPTOR_HANDLE ) a, DFTI_REAL_STORAGE, DFTI_REAL_REAL );
				status = DftiSetValue( ( DFTI_DESCRIPTOR_HANDLE ) a, DFTI_CONJUGATE_EVEN_STORAGE, DFTI_COMPLEX_REAL );
				status = DftiSetValue( ( DFTI_DESCRIPTOR_HANDLE ) a, DFTI_PACKED_FORMAT, DFTI_PACK_FORMAT );
				status = DftiSetValue( ( DFTI_DESCRIPTOR_HANDLE ) a, DFTI_BACKWARD_SCALE, 1.0F / ( float ) n );
				status = DftiCommitDescriptor( ( DFTI_DESCRIPTOR_HANDLE ) a );
				if( status == DFTI_NO_ERROR ) return true;
				else return false;
#elif defined( FFT_USE_FFTPACK )
				a = ( void * ) new float[ 2 * n + 15 ];
				b = ( void * ) new int;
				*( ( int * ) b ) = n;
				rffti_( &n, ( float * ) a );
				return true;
#else
				return false;
#endif
			}

			bool inline ComputeForwardTransform( float *data )
			{
#if defined( FFT_USE_MKL )
				int status = DftiComputeForward( ( DFTI_DESCRIPTOR_HANDLE ) a, data );
				if( status == DFTI_NO_ERROR ) return true;
				else return false;
#elif defined( FFT_USE_FFTPACK )
				rfftf_( ( int * ) b, data, ( float * ) a );
				return true;
#else
				return false;
#endif
			}

			bool inline ComputeReverseTransform( float *data )
			{
#if defined( FFT_USE_MKL )
				int status = DftiComputeBackward( ( DFTI_DESCRIPTOR_HANDLE ) a, data );
				if( status == DFTI_NO_ERROR ) return true;
				else return false;
#elif defined( FFT_USE_FFTPACK )
				rfftb_( ( int * ) b, data, ( float * ) a );
				return true;
#else
				return false;
#endif
			}

		private:

			void *a;
			void *b;
			void *c;
	};
}

#endif		// BEC_DFT_INCLUDED

/*  End becdft.h  */

